英文牌名	Aura Blast
中文牌名	靈氣衝擊波
魔法力費用	1白
牌的類別	瞬間
畫家	Ron Walotsky
編號	1
規則敘述	消滅目標結界。
規則敘述	抽一張牌。
背景敘述	「你們的歷史課本都沒提過我嗎？」達莉亞笑道：「現在它們會提了吧。」
	
英文牌名	Aurora Griffin
中文牌名	曙光獅鷲
魔法力費用	3白
牌的類別	生物～獅鷲
畫家	Ciruelo
編號	2
力量／防禦力	2/2
規則敘述	飛行
規則敘述	oW：目標永久物成為白色直到回合結束。
背景敘述	牠的確凶猛善戰，但最可貴的是牠帶給受圍困軍隊激昂的士氣。
	
英文牌名	Disciple of Kangee
中文牌名	坎鴶的門徒
魔法力費用	2白
牌的類別	生物～魔法師
畫家	Wayne England
編號	3
力量／防禦力	2/2
規則敘述	oU，ocT：目標生物獲得飛行異能，並成為藍色，直到回合結束。
背景敘述	當沒有鳥可使喚時，坎鴶的門徒便將任何願意戰鬥的東西送上天空。
	
英文牌名	Dominaria's Judgment
中文牌名	多明納里亞大審判
魔法力費用	2白
牌的類別	瞬間
畫家	John Avon
編號	4
規則敘述	直到回合結束，由你操控的生物獲得以下反色保護異能：若你操控平原則反白；若你操控海島則反藍；若你操控沼澤則反黑；若你操控山脈則反紅；若你操控樹林則反綠。
	
英文牌名	Guard Dogs
中文牌名	警衛狗
魔法力費用	3白
牌的類別	生物～獵犬
畫家	Mike Raabe
編號	5
力量／防禦力	2/2
規則敘述	o2oW，ocT：選擇一個由你操控的永久物。若目標生物與此永久物有共通的顏色，則於本回合中，防止它將造成的所有戰鬥傷害。
背景敘述	「高明的警衛為你時時留神，優秀的警衛則還要對你忠誠。」
背景敘述	～賓納里亞的育犬家
	
英文牌名	Heroic Defiance
中文牌名	英勇挑戰
魔法力費用	1白
牌的類別	生物結界
畫家	Terese Nielsen
編號	6
規則敘述	若所有永久物之間最普遍的顏色或同為最普遍的數個顏色中，和受此結界的生物沒有共通的顏色，則它得+3/+3。
背景敘述	「將勇氣如甲冑般穿戴，將榮譽如刀劍般揮舞。」
背景敘述	～傑拉爾德
	
英文牌名	Hobble
中文牌名	蹣跚
魔法力費用	2白
牌的類別	生物結界
畫家	Alan Pollack
編號	7
規則敘述	當蹣跚進場時，抽一張牌。
規則敘述	受此結界的生物不能進行攻擊。
規則敘述	受此結界的生物若為黑色，則不能進行阻擋。
	
英文牌名	Honorable Scout
中文牌名	可敬的斥侯
魔法力費用	白
牌的類別	生物～士兵
畫家	Mike Ploog
編號	8
力量／防禦力	1/1
規則敘述	當可敬的斥侯進場時，目標對手每操控一個黑色和／或紅色生物，你便獲得2點生命。
背景敘述	「我與你那邊的人打過交道。這次我可是有備而來。」
	
英文牌名	Lashknife Barrier
中文牌名	鎖鏈刀舞
魔法力費用	2白
牌的類別	結界
畫家	Paolo Parente
編號	9
規則敘述	當鎖鍊刀舞進場時，抽一張牌。
規則敘述	若任一來源將對一個由你操控的生物造成傷害，則改為該來源對該生物造成原數量減1的傷害。
	
英文牌名	March of Souls
中文牌名	眾魂行進
魔法力費用	4白
牌的類別	巫術
畫家	Marc Fishman
編號	10
規則敘述	消滅所有生物。它們不能重生。每有一個生物因此被消滅，其操控者便將一個1/1白色，具有飛行異能的精靈衍生物放置進場。
	
英文牌名	Orim's Chant
中文牌名	歐琳的吟頌
魔法力費用	白
牌的類別	瞬間
畫家	Kev Walker
編號	11
規則敘述	增幅oW＃（你使用此咒語時可以額外支付oW）＃
規則敘述	目標玩家本回合不能使用咒語。
規則敘述	若你已支付其增幅費用，則本回合生物亦不能攻擊。
	
英文牌名	Planeswalker's Mirth
中文牌名	旅法師的歡顏
魔法力費用	2白
牌的類別	結界
畫家	John Matson
編號	12
規則敘述	o3oW：目標對手從手上隨機展示一張牌。你獲得與該牌之總魔法力費用等量的生命。
背景敘述	「我和普通人一樣喜歡好笑話。」
背景敘述	～古夫准將
	
英文牌名	Pollen Remedy
中文牌名	花粉療法
魔法力費用	白
牌的類別	瞬間
畫家	Ben Thompson
編號	13
規則敘述	增幅～犧牲一張地＃（你使用此咒語時可以額外犧牲一張地）＃。
規則敘述	於本回合中，為任意數量的目標生物和／或玩家防止接下來的3點傷害，你可以任意分配。若你已支付其增幅費用，則改為以此法防止接下來的6點傷害。
	
英文牌名	Samite Elder
中文牌名	撒姆尼長老
魔法力費用	2白
牌的類別	生物～僧侶
畫家	Terese Nielsen
編號	14
力量／防禦力	1/2
規則敘述	ocT：直到回合結束，由你操控的生物獲得反色保護異能，該些顏色為目標由你操控之永久物所具有的顏色。
	
英文牌名	Samite Pilgrim
中文牌名	撒姆尼朝聖客
魔法力費用	1白
牌的類別	生物～僧侶
畫家	D. J. Cleland-Hura
編號	15
力量／防禦力	1/1
規則敘述	ocT：於本回合中，為目標生物防止接下來的Ｘ點傷害；Ｘ為在由你操控的地之中，基本地類別的數量。
	
英文牌名	Sunscape Battlemage
中文牌名	陽景院戰巫師
魔法力費用	2白
牌的類別	生物～魔法師
畫家	Tony Szczudlo
編號	16
力量／防禦力	2/2
規則敘述	增幅o1oG和／或o2oU
規則敘述	當陽景院戰巫師進場時，若你已支付其o1oG的增幅費用，則消滅目標具有飛行異能的生物。
規則敘述	當陽景院戰巫師進場時，若你已支付其o2oU的增幅費用，則抽兩張牌。
	
英文牌名	Sunscape Familiar
中文牌名	陽景院傭獸
魔法力費用	1白
牌的類別	生物～牆
畫家	Brian Despain
編號	17
力量／防禦力	0/3
規則敘述	＃（牆不能攻擊）＃
規則敘述	你所使用的綠色和藍色咒語減少o1即可使用。
背景敘述	戰巫師死後的靈魂，能依附在各種形體上成為傭獸，繼續為公會效力。
	
英文牌名	Surprise Deployment
中文牌名	奇兵
魔法力費用	3白
牌的類別	瞬間
畫家	Bradley Williams
編號	18
規則敘述	你只能於戰鬥中使用奇兵。
規則敘述	將一張非白色的生物牌從你手上放置進場。在回合結束時，將該生物移回你的手上。＃（若它在場上，你才須將之移回）＃
	
英文牌名	Voice of All
中文牌名	萬物使者
魔法力費用	2白白
牌的類別	生物～天使
畫家	rk post
編號	19
力量／防禦力	2/2
規則敘述	飛行
規則敘述	於萬物使者進場時，選擇一個顏色。
規則敘述	萬物使者具有該色的反色保護異能。
	
英文牌名	Allied Strategies
中文牌名	結盟策略
魔法力費用	4藍
牌的類別	巫術
畫家	Paolo Parente
編號	20
規則敘述	在目標玩家所操控的地之中，每有一種基本地類別，他便抽一張牌。
背景敘述	指揮官們面面相覷，等著看誰會帶頭背棄盟約。
	
英文牌名	Arctic Merfolk
中文牌名	極地人魚
魔法力費用	1藍
牌的類別	生物～人魚
畫家	Ron Spears
編號	21
力量／防禦力	1/1
規則敘述	增幅～將一個由你操控的生物移回其擁有者手上（你使用此咒語時可以額外將一個由你操控的生物移回其擁有者手上）。
規則敘述	若你已支付其增幅費用，則極地人魚進場時上面有一個+1/+1指示物。
	
英文牌名	Confound
中文牌名	混淆
魔法力費用	1藍
牌的類別	瞬間
畫家	Doug Chaffee
編號	22
規則敘述	反擊目標指定一個或更多生物為目標的咒語。
規則敘述	抽一張牌。
背景敘述	「你問我之前我知不知道剎特會背叛我們？」克撒似笑非笑地說：「我還指望著呢。」
	
英文牌名	Dralnu's Pet
中文牌名	卓爾努的寵物
魔法力費用	1藍藍
牌的類別	生物～變形獸
畫家	Glen Angus
編號	23
力量／防禦力	2/2
規則敘述	增幅～o2oB，從你手上棄掉一張生物牌＃（你使用此咒語時可以額外支付o2oB並從你手上棄掉一張生物牌）＃。
規則敘述	若你已支付其增幅費用，則卓爾努的寵物具有飛行異能，並且進場時上面有Ｘ個+1/+1指示物，Ｘ為所棄掉之牌的總魔法力費用。
	
英文牌名	Ertai's Trickery
中文牌名	爾泰的詭計
魔法力費用	藍
牌的類別	瞬間
畫家	Kev Walker
編號	24
規則敘述	若有增幅費用為目標咒語而支付，則反擊之。
背景敘述	「傑拉爾德，別擔心，」爾泰說：「我相信他們趕來救你的速度會和之前來救我的一樣快。」
	
英文牌名	Escape Routes
中文牌名	逃脫路線
魔法力費用	2藍
牌的類別	結界
畫家	Marc Fishman
編號	25
規則敘述	o2oU：將目標由你操控的白色或黑色生物移回其擁有者手上。
背景敘述	「我會帶你脫離險境…或是帶你到暫時還不算太危險的地帶。」
背景敘述	～烏爾博格擺渡舵手
	
英文牌名	Gainsay
中文牌名	駁斥
魔法力費用	1藍
牌的類別	瞬間
畫家	Roger Raupp
編號	26
規則敘述	反擊目標藍色咒語。
背景敘述	「只要你是對的，我馬上就停止對你的反駁，克撒。」
背景敘述	～旅法師波‧里瓦
	
英文牌名	Hunting Drake
中文牌名	行獵龍獸
魔法力費用	4藍
牌的類別	生物～龍獸
畫家	Wayne England
編號	27
力量／防禦力	2/2
規則敘述	飛行
規則敘述	當行獵龍獸進場時，將目標紅色或綠色生物置於其擁有者的牌庫頂上。
背景敘述	就算是最兇猛的野獸，在面對可能變成晚餐的狀況時都會逃開。
	
英文牌名	Planar Overlay
中文牌名	時空覆蓋
魔法力費用	2藍
牌的類別	巫術
畫家	Ron Walotsky
編號	28
規則敘述	每位玩家在他所操控的地之中，對每一種基本地類別各選擇一張。將這些地移回其擁有者手上。
背景敘述	非瑞克西亞人並非要佔領多明納里亞。他們是想重做一個。
	
英文牌名	Planeswalker's Mischief
中文牌名	旅法師的惡作劇
魔法力費用	2藍
牌的類別	結界
畫家	Pete Venters
編號	29
規則敘述	o3oU：目標對手從手上隨機展示一張牌。若該牌是巫術或瞬間牌，則將之移出遊戲。只要該牌保持在被移出遊戲的狀態，你便可將它視為在你手上一般地使用，且不須支付其魔法力費用。若該牌之魔法力費用中含有Ｘ，則Ｘ為0。在回合結束時，若你仍未使用該牌，則將之移回其擁有者手上。你只可以於你能夠使用巫術的時機下使用此異能。
	
英文牌名	Rushing River
中文牌名	湍流
魔法力費用	2藍
牌的類別	瞬間
畫家	Don Hazeltine
編號	30
規則敘述	增幅～犧牲一張地＃（你使用此咒語時可以額外犧牲一張地）＃。
規則敘述	將目標非地的永久物移回其擁有者手上。若你已支付其增幅費用，則再將另一個目標非地的永久物移回其擁有者手上。
	
英文牌名	Sea Snidd
中文牌名	海生史尼獸
魔法力費用	4藍
牌的類別	生物～野獸
畫家	Chippy
編號	31
力量／防禦力	3/3
規則敘述	ocT：選擇一種基本地類別，目標地成為該類別的地直到回合結束。
背景敘述	牠無論何時都能占有地利。
	
英文牌名	Shifting Sky
中文牌名	變幻長空
魔法力費用	2藍
牌的類別	結界
畫家	Jerry Tiritilli
編號	32
規則敘述	於變幻長空進場時，選擇一個顏色。
規則敘述	所有非地的永久物成為該色。
背景敘述	掌握天空，即能掌握其下的一切。
背景敘述	～仿索藍人銘言
	
英文牌名	Sisay's Ingenuity
中文牌名	西賽的巧思
魔法力費用	藍
牌的類別	生物結界
畫家	Paolo Parente
編號	33
規則敘述	當西賽的巧思進場時，抽一張牌。
規則敘述	受此結界的生物具有「o2oU：選擇一個顏色，目標生物成為該色直到回合結束。」
	
英文牌名	Sleeping Potion
中文牌名	催眠劑
魔法力費用	1藍
牌的類別	生物結界
畫家	Daren Bader
編號	34
規則敘述	當催眠劑進場時，橫置受此結界的生物。
規則敘述	受此結界的生物於其操控者的重置步驟中不可重置。
規則敘述	當受此結界的生物成為咒語或異能的目標時，犧牲催眠劑。
	
英文牌名	Stormscape Battlemage
中文牌名	嵐景院戰巫師
魔法力費用	2藍
牌的類別	生物～魔法師
畫家	Christopher Moeller
編號	35
力量／防禦力	2/2
規則敘述	增幅oW和／或 o2oB
規則敘述	當嵐景院戰巫師進場時，若你已支付其oW的增幅費用，則你獲得3點生命。
規則敘述	當嵐景院戰巫師進場時，若你已支付其o2oB的增幅費用，則消滅目標非黑色的生物。該生物不能重生。
	
英文牌名	Stormscape Familiar
中文牌名	嵐景院傭獸
魔法力費用	1藍
牌的類別	生物～鳥
畫家	Heather Hudson
編號	36
力量／防禦力	1/1
規則敘述	飛行
規則敘述	你所使用的白色和黑色咒語減少o1即可使用。
背景敘述	嵐景院的學徒在升格為大師以前，都必須親手養育一隻貓頭鷹。
	
英文牌名	Sunken Hope
中文牌名	沈淪的希望
魔法力費用	3藍藍
牌的類別	結界
畫家	Greg Staples
編號	37
規則敘述	在每位玩家的維持開始時，該玩家將一個由他操控的生物移回其擁有者手上。
背景敘述	多明納里亞人為他們的世界奮鬥著，而瑞斯人則由他們的腳下偷盜走。
	
英文牌名	Waterspout Elemental
中文牌名	水龍捲元素
魔法力費用	3藍藍
牌的類別	生物～元素
畫家	Mark Romanoski
編號	38
力量／防禦力	3/4
規則敘述	增幅oU＃（你使用此咒語時可以額外支付oU）＃
規則敘述	飛行
規則敘述	當水龍捲元素進場時，若你已支付其增幅費用，則將所有其他生物移回其擁有者手上，並且你略過你的下一個回合。
	
英文牌名	Bog Down
中文牌名	腐沼沈淪
魔法力費用	2黑
牌的類別	巫術
畫家	Andrew Goldhawk
編號	39
規則敘述	增幅～犧牲兩張地＃（你使用此咒語時可以額外犧牲兩張地）＃
規則敘述	目標玩家從手上棄掉兩張牌。若你已支付其增幅費用，則改為該玩家從手上棄掉三張牌。
	
英文牌名	Dark Suspicions
中文牌名	邪疑
魔法力費用	2黑黑
牌的類別	結界
畫家	Matt Cavotta
編號	40
規則敘述	在每位對手的維持開始時，該玩家的手牌每比你的手牌多一張，他便失去1點生命。
背景敘述	「四千年的歲月，讓你學到了如何謀反。」
背景敘述	～克撒
	
英文牌名	Death Bomb
中文牌名	死亡炸彈
魔法力費用	3黑
牌的類別	瞬間
畫家	Dan Frazier
編號	41
規則敘述	犧牲一個生物，以作為使用死亡炸彈的額外費用。
規則敘述	消滅目標非黑色的生物。它不能重生。其操控者失去2點生命。
背景敘述	為約格莫夫的輝煌美夢而死，是非瑞克西亞的最高榮譽。
	
英文牌名	Diabolic Intent
中文牌名	惡魔意圖
魔法力費用	1黑
牌的類別	巫術
畫家	Dave Dorman
編號	42
規則敘述	犧牲一個生物，以作為使用惡魔意圖的額外費用。
規則敘述	從你的牌庫中搜尋一張牌，並將該牌置入你手上。然後將你的牌庫洗牌。
	
英文牌名	Exotic Disease
中文牌名	異地疾病
魔法力費用	4黑
牌的類別	巫術
畫家	Kev Walker
編號	43
規則敘述	目標玩家失去Ｘ點生命，並且你獲得Ｘ點生命，Ｘ為在由你操控的地之中，基本地類別的數量。
背景敘述	「別怕死亡，我會將你從永世的束縛中解放，讓你獲得榮耀。」
背景敘述	～卓爾努領主
	
英文牌名	Lord of the Undead
中文牌名	不死生物大帝
魔法力費用	1黑黑
牌的類別	生物～領主
畫家	Brom
編號	44
力量／防禦力	2/2
規則敘述	所有僵屍得+1/+1。
規則敘述	o1oB，ocT：將目標在你墳墓場中的僵屍牌移回你的手上。
背景敘述	「我與死亡打交道。你可有本事傷得了我？」
背景敘述	～卓爾努領主
	
英文牌名	Maggot Carrier
中文牌名	病蛆帶原體
魔法力費用	黑
牌的類別	生物～僵屍
畫家	Ron Spencer
編號	45
力量／防禦力	1/1
規則敘述	當病蛆帶原體進場時，每位玩家各失去1點生命。
背景敘述	「我光看到我們的不死盟友就想吐。你到底做了什麼骯髒買賣？」
背景敘述	～格利澤甘問阿格內特
	
英文牌名	Morgue Toad
中文牌名	屍殿蟾蜍
魔法力費用	2黑
牌的類別	生物～蟾蜍
畫家	Franz Vohwinkel
編號	46
力量／防禦力	2/2
規則敘述	犧牲屍殿蟾蜍：加oUoR到你的魔法力池中。
背景敘述	「烏爾博格的蟾蜍速度不快也不強，還令人噁心，但卻另有妙用。」
背景敘述	～爾泰
	
英文牌名	Nightscape Battlemage
中文牌名	夜景院戰巫師
魔法力費用	2黑
牌的類別	生物～魔法師
畫家	Andrew Goldhawk
編號	47
力量／防禦力	2/2
規則敘述	增幅o2oU和／或o2oR
規則敘述	當夜景院戰巫師進場時，若你已支付其o2oU的增幅費用，則將至多兩個目標非黑色的生物移回其擁有者手上。
規則敘述	當夜景院戰巫師進場時，若你已支付其o2oR的增幅費用，則消滅目標地。
	
英文牌名	Nightscape Familiar
中文牌名	夜景院傭獸
魔法力費用	1黑
牌的類別	生物～僵屍
畫家	Jeff Easley
編號	48
力量／防禦力	1/1
規則敘述	你所使用的藍色和紅色咒語減少o1即可使用。
規則敘述	o1oB：重生夜景院傭獸。
背景敘述	夜景院大師不只喚醒死亡戰巫師的魂魄，連肉體也一起復甦。
	
英文牌名	Noxious Vapors
中文牌名	腐臭蒸汽
魔法力費用	1黑黑
牌的類別	巫術
畫家	Ben Thompson
編號	49
規則敘述	每位玩家展示其手牌，並且從中對每個顏色各選擇一張。然後棄掉所有其他非地的牌。
背景敘述	烏爾博格很快就會把你的記憶連同生命一起偷走。
	
英文牌名	Phyrexian Bloodstock
中文牌名	非瑞克西亞血畜
魔法力費用	4黑
牌的類別	生物～僵屍
畫家	Mark Tedin
編號	50
力量／防禦力	3/3
規則敘述	當非瑞克西亞血畜離場時，消滅目標白色生物。該生物不能重生。
背景敘述	它瀰漫著恐懼和混亂。
	
英文牌名	Phyrexian Scuta
中文牌名	非瑞克西亞盾甲隊
魔法力費用	3黑
牌的類別	生物～僵屍
畫家	Scott M. Fischer
編號	51
力量／防禦力	3/3
規則敘述	增幅～支付3點生命＃（你使用此咒語時可以額外支付3點生命）＃。
規則敘述	若你已支付其增幅費用，則非瑞克西亞盾甲隊進場時上面有兩個+1/+1指示物。
	
英文牌名	Planeswalker's Scorn
中文牌名	旅法師的輕蔑
魔法力費用	2黑
牌的類別	結界
畫家	Glen Angus
編號	52
規則敘述	o3oB：目標對手從手上隨機展示一張牌。目標生物得-Ｘ/-Ｘ直到回合結束，Ｘ為所展示之牌的總魔法力費用。你只可以於你能夠使用巫術的時機下使用此異能。
	
英文牌名	Shriek of Dread
中文牌名	懾魂尖鳴
魔法力費用	1黑
牌的類別	瞬間
畫家	Nelson DeCastro
編號	53
規則敘述	目標生物本回合只能被神器生物和／或黑色生物阻擋。
背景敘述	當克羅希斯嚎叫時，戰鬥便結束而屠殺正開始。
	
英文牌名	Sinister Strength
中文牌名	不祥之力
魔法力費用	1黑
牌的類別	生物結界
畫家	Terese Nielsen
編號	54
規則敘述	受此結界的生物得+3/+1，並且成為黑色。
背景敘述	「以死之歌祝福生者。」
背景敘述	～卓爾努領主
	
英文牌名	Slay
中文牌名	戕殺
魔法力費用	2黑
牌的類別	瞬間
畫家	Ben Thompson
編號	55
規則敘述	消滅目標綠色生物。它不能重生。
規則敘述	抽一張牌。
背景敘述	妖精們有智謀、勇氣和技巧，但到頭來只證明了數量還是最重要的。
	
英文牌名	Volcano Imp
中文牌名	火山小惡魔
魔法力費用	3黑
牌的類別	生物～小惡魔
畫家	Thomas M. Baxa
編號	56
力量／防禦力	2/2
規則敘述	飛行
規則敘述	o1oR：火山小惡魔獲得先攻異能直到回合結束。
背景敘述	牠的利爪快如火影搖曳。
	
英文牌名	Warped Devotion
中文牌名	扭曲奉獻
魔法力費用	2黑
牌的類別	結界
畫家	Orizio Daniele
編號	57
規則敘述	每當任一永久物被移回玩家手上時，該玩家從手上棄掉一張牌。
背景敘述	「在約格莫夫的榮耀下，沒錯，即使如此也是合理的。」
背景敘述	～克撒致傑拉爾德
	
英文牌名	Caldera Kavu
中文牌名	火山口卡甫
魔法力費用	2紅
牌的類別	生物～卡甫
畫家	Arnie Swekel
編號	58
力量／防禦力	2/2
規則敘述	o1oB：火山口卡甫得+1/+1直到回合結束。
規則敘述	oG：選擇一個顏色，火山口卡甫成為該色直到回合結束。
背景敘述	既堅硬又可鍛鍊，如同家鄉的鑄石。
	
英文牌名	Deadapult
中文牌名	投屍器
魔法力費用	2紅
牌的類別	結界
畫家	Mark Brill
編號	59
規則敘述	oR，犧牲一個僵屍：投屍器對目標生物或玩家造成2點傷害。
背景敘述	「冒火的僵屍是最麻煩的東西。」
背景敘述	～坦格爾斯
	
英文牌名	Flametongue Kavu
中文牌名	焰舌卡甫
魔法力費用	3紅
牌的類別	生物～卡甫
畫家	Pete Venters
編號	60
力量／防禦力	4/2
規則敘述	當焰舌卡甫進場時，它對目標生物造成4點傷害。
背景敘述	「雖然是笨笨呆呆的突變種，但是牠們卻瞄得非常準。」
背景敘述	～西賽
	
英文牌名	Goblin Game
中文牌名	鬼怪遊戲
魔法力費用	5紅紅
牌的類別	巫術
畫家	DiTerlizzi
編號	61
規則敘述	每位玩家藏匿至少一個物品，然後所有玩家同時展示所藏物品。每位玩家失去等同於自己所藏物品數量的生命。然後展示最少物品的玩家失去一半生命，小數點以下進位。若有多位玩家同為最少，則其中每位玩家都失去一半生命，小數點以下進位。
	
英文牌名	Implode
中文牌名	內爆
魔法力費用	4紅
牌的類別	巫術
畫家	Arnie Swekel
編號	62
規則敘述	消滅目標地。
規則敘述	抽一張牌。
背景敘述	為了拯救多明納里亞，九位旅法師試圖毀滅非瑞克西亞。
	
英文牌名	Insolence
中文牌名	粗野無禮
魔法力費用	2紅
牌的類別	生物結界
畫家	Carl Critchlow
編號	63
規則敘述	每當受此結界的生物成為橫置時，粗野無禮便對該生物的操控者造成2點傷害。
背景敘述	「你那非得犧牲凡間的龍才能滿足的龐大飢渴，就到此為止。」
背景敘述	～達里迦告誡麗絲
	
英文牌名	Kavu Recluse
中文牌名	隱遁卡甫
魔法力費用	2紅
牌的類別	生物～卡甫
畫家	Aaron Boyd
編號	64
力量／防禦力	2/2
規則敘述	ocT：目標地成為樹林直到回合結束。
背景敘述	很少人見過這種卡甫，牠們住在自己建造的森林中。
	
英文牌名	Keldon Mantle
中文牌名	凱爾頓披風
魔法力費用	1紅
牌的類別	生物結界
畫家	Rebecca Guay
編號	65
規則敘述	oB：重生受此結界的生物。
規則敘述	oR：受此結界的生物得+1/+0直到回合結束。
規則敘述	oG：受此結界的生物獲得踐踏異能直到回合結束。
	
英文牌名	Magma Burst
中文牌名	岩漿爆
魔法力費用	3紅
牌的類別	瞬間
畫家	Bradley Williams
編號	66
規則敘述	增幅～犧牲兩張地＃（你使用此咒語時可以額外犧牲兩張地）＃
規則敘述	岩漿爆對目標生物或玩家造成3點傷害。若你已支付其增幅費用，則岩漿爆再對另一個目標生物或玩家造成3點傷害。
	
英文牌名	Mire Kavu
中文牌名	泥沼卡甫
魔法力費用	3紅
牌的類別	生物～卡甫
畫家	Wayne England
編號	67
力量／防禦力	3/2
規則敘述	只要你操控沼澤，泥沼卡甫便得+1/+1。
背景敘述	「以這種速率來看，」西賽道：「接下來我們會看到長翅膀的卡甫。」
	
英文牌名	Mogg Jailer
中文牌名	莫葛獄卒
魔法力費用	1紅
牌的類別	生物～鬼怪
畫家	Mark Romanoski
編號	68
力量／防禦力	2/2
規則敘述	若防禦玩家操控力量小於或等於2的未橫置生物，莫葛獄卒便不能進行攻擊。
背景敘述	喂，快醒醒！
背景敘述	～莫葛口令
	
英文牌名	Mogg Sentry
中文牌名	莫葛哨兵
魔法力費用	紅
牌的類別	生物～鬼怪
畫家	"Edward P. Beard, Jr."
編號	69
力量／防禦力	1/1
規則敘述	每當有對手使用咒語時，莫葛哨兵便得+2/+2直到回合結束。
背景敘述	「沒人佔得到比莫葛更好的位置。」
背景敘述	～坦格爾斯
	
英文牌名	Planeswalker's Fury
中文牌名	旅法師的怒火
魔法力費用	2紅
牌的類別	結界
畫家	Christopher Moeller
編號	70
規則敘述	o3oR：目標對手從手上隨機展示一張牌。旅法師的怒火對該玩家造成與該牌的總魔法力費用等量的傷害。你只可以於你能夠使用巫術的時機下使用此異能。
	
英文牌名	Singe
中文牌名	焦燙
魔法力費用	紅
牌的類別	瞬間
畫家	John Avon
編號	71
規則敘述	焦燙對目標生物造成1點傷害。該生物成為黑色直到回合結束。
背景敘述	燙傷肉體，焦灼靈魂。
	
英文牌名	Slingshot Goblin
中文牌名	彈弓鬼怪
魔法力費用	2紅
牌的類別	生物～鬼怪
畫家	Jeff Easley
編號	72
力量／防禦力	2/2
規則敘述	oR，ocT：彈弓鬼怪對目標藍色生物造成2點傷害。
背景敘述	只要他不把彈藥吃掉，就非常有用處。
	
英文牌名	Strafe
中文牌名	猛烈炮轟
魔法力費用	紅
牌的類別	巫術
畫家	Jim Nelson
編號	73
規則敘述	猛烈炮轟對目標非紅色的生物造成3點傷害。
背景敘述	「弟兄們，用火光染紅敵人吧！」
背景敘述	～傑拉爾德
	
英文牌名	"Tahngarth, Talruum Hero"
中文牌名	塔路姆英雄坦格爾斯
魔法力費用	3紅紅
牌的類別	生物～傳奇牛頭怪
畫家	Dave Dorman
編號	74
力量／防禦力	4/4
規則敘述	塔路姆英雄坦格爾斯攻擊時不須橫置。
規則敘述	o1oR，ocT：坦格爾斯對目標生物造成等同於坦格爾斯力量的傷害。該生物對坦格爾斯造成等同於該生物力量的傷害。
	
英文牌名	Thunderscape Battlemage
中文牌名	雷景院戰巫師
魔法力費用	2紅
牌的類別	生物～魔法師
畫家	Mike Ploog
編號	75
力量／防禦力	2/2
規則敘述	增幅o1oB和／或oG
規則敘述	當雷景院戰巫師進場時，若你已支付其o1oB的增幅費用，則目標玩家從手上棄掉兩張牌。
規則敘述	當雷景院戰巫師進場時，若你已支付其oG的增幅費用，則消滅目標結界。
	
英文牌名	Thunderscape Familiar
中文牌名	雷景院傭獸
魔法力費用	1紅
牌的類別	生物～卡甫
畫家	Daren Bader
編號	76
力量／防禦力	1/1
規則敘述	先攻
規則敘述	你所使用的黑色和綠色咒語減少o1即可使用。
背景敘述	雷景院大師放出傭獸，以帶領學徒走向雷景院之道。
	
英文牌名	Alpha Kavu
中文牌名	原型卡甫
魔法力費用	2綠
牌的類別	生物～卡甫
畫家	Matt Cavotta
編號	77
力量／防禦力	2/2
規則敘述	o1oG：目標卡甫得-1/+1直到回合結束。
背景敘述	卡甫的領袖會本能地保護其群體避免受傷。
	
英文牌名	Amphibious Kavu
中文牌名	兩棲卡甫
魔法力費用	2綠
牌的類別	生物～卡甫
畫家	Wayne England
編號	78
力量／防禦力	2/2
規則敘述	每當兩棲卡甫阻擋藍色和／或黑色生物，或被藍色和／或黑色生物阻擋時，兩棲卡甫便得+3/+3直到回合結束。
背景敘述	人魚在海岸恣意掠奪...直到海岸開始還擊。
	
英文牌名	Falling Timber
中文牌名	落木
魔法力費用	2綠
牌的類別	瞬間
畫家	Eric Peterson
編號	79
規則敘述	增幅～犧牲一張地＃（你使用此咒語時可以額外犧牲一張地）＃。
規則敘述	於本回合中，防止目標生物將造成的所有戰鬥傷害。若你已支付其增幅費用，則於本回合中，再防止另一個目標生物將造成的所有戰鬥傷害。
	
英文牌名	Gaea's Herald
中文牌名	蓋亞傳令使
魔法力費用	1綠
牌的類別	生物～妖精
畫家	Dan Frazier
編號	80
力量／防禦力	1/1
規則敘述	生物咒語不能被咒語或異能反擊。
背景敘述	「我帶來蓋亞的口信。戰鬥下去吧！她絕不會讓她的孩子孤單赴死。」
	
英文牌名	Gaea's Might
中文牌名	蓋亞之力
魔法力費用	綠
牌的類別	瞬間
畫家	Ron Spencer
編號	81
規則敘述	在由你操控的地之中，每有一種基本地類別，目標生物便得+1/+1直到回合結束。
背景敘述	有了亞維馬雅之心的援助，穆塔尼的火力從不吃緊。
	
英文牌名	Magnigoth Treefolk
中文牌名	馬尼古斯樹妖
魔法力費用	4綠
牌的類別	生物～樹妖
畫家	Peter Bollinger
編號	82
力量／防禦力	2/6
規則敘述	在由你操控的地之中，每有一種基本地類別，馬尼古斯樹妖便具有該類別的地行者異能。＃（只要防禦玩家操控該類別的地，則此生物無法被阻擋）＃
	
英文牌名	Mirrorwood Treefolk
中文牌名	鏡木林樹妖
魔法力費用	3綠
牌的類別	生物～樹妖
畫家	Arnie Swekel
編號	83
力量／防禦力	2/4
規則敘述	o2oRoW：於本回合中，當鏡木林樹妖下一次將受到傷害時，該傷害改為對目標生物或玩家造成之。
背景敘述	牠不用咬，光樹皮就夠了。
	
英文牌名	Multani's Harmony
中文牌名	穆塔尼的和諧
魔法力費用	綠
牌的類別	生物結界
畫家	Darrell Riche
編號	84
規則敘述	受此結界的生物具有「ocT：加一點任意顏色的魔法力到你的魔法力池中。」
背景敘述	「歡迎駕臨天堂。」
	
英文牌名	"Nemata, Grove Guardian"
中文牌名	叢林守護神奈瑪塔
魔法力費用	4綠綠
牌的類別	生物～傳奇樹妖
畫家	John Avon
編號	85
力量／防禦力	4/5
規則敘述	o2oG：將一個1/1綠色腐生物衍生物放置進場。
規則敘述	犧牲一個腐生物：所有腐生物得+1/+1直到回合結束。
	
英文牌名	Planeswalker's Favor
中文牌名	旅法師的恩惠
魔法力費用	2綠
牌的類別	結界
畫家	Rebecca Guay
編號	86
規則敘述	o3oG：目標對手從手上隨機展示一張牌。目標生物得+X/+X直到回合結束，X為所展示之牌的總魔法力費用。
背景敘述	妃雅麗玆為其信者帶來力量。
	
英文牌名	Primal Growth
中文牌名	原初成長
魔法力費用	2綠
牌的類別	巫術
畫家	rk post
編號	87
規則敘述	增幅～犧牲一個生物＃（你使用此咒語時可以額外犧牲一個生物）＃。
規則敘述	從你的牌庫中搜尋一張基本地牌，將該牌放置進場，然後將你的牌庫洗牌。若你已支付其增幅費用，則改為從你的牌庫中搜尋兩張基本地牌，將它們放置進場，然後將你的牌庫洗牌。
	
英文牌名	Pygmy Kavu
中文牌名	矮黑卡甫
魔法力費用	3綠
牌的類別	生物～卡甫
畫家	Greg Staples
編號	88
力量／防禦力	1/2
規則敘述	當矮黑卡甫進場時，對手每操控一個黑色生物，你便抽一張牌。
背景敘述	「觀察這些小傢伙，可以讓人學到很多東西。」阿格內特說道。
背景敘述	「可以請教你對『小』的定義嗎？」格利澤岡說道。
	
英文牌名	Quirion Dryad
中文牌名	奎利恩樹靈
魔法力費用	1綠
牌的類別	生物～樹靈
畫家	Don Hazeltine
編號	89
力量／防禦力	1/1
規則敘述	當你使用白色、藍色、黑色、或紅色咒語時，放置一個+1/+1指示物在奎利恩樹靈上。
背景敘述	別低估大自然對人為影響的適應能力。
背景敘述	～瑪洛術士莫黎墨
	
英文牌名	Quirion Explorer
中文牌名	奎利恩探險家
魔法力費用	1綠
牌的類別	生物～妖精
畫家	Ron Spears
編號	90
力量／防禦力	1/1
規則敘述	ocT：加一點魔法力到你的魔法力池中，其顏色為由對手操控的地能產生之任一顏色。
背景敘述	與你藤上的魔法、手中的鋼鐵與身後的朋友共同奮戰。
背景敘述	～奎利恩教條
	
英文牌名	Root Greevil
中文牌名	根叢葛雷猧
魔法力費用	3綠
牌的類別	生物～野獸
畫家	Andrew Robinson
編號	91
力量／防禦力	2/3
規則敘述	o2oG，ocT，犧牲根叢葛雷猧：選擇一個顏色，消滅所有該色的結界。
背景敘述	所有葛雷猧的根。
	
英文牌名	Skyshroud Blessing
中文牌名	天帷之禱
魔法力費用	1綠
牌的類別	瞬間
畫家	Jerry Tiritilli
編號	92
規則敘述	於本回合中，地不能成為咒語或異能的目標。
規則敘述	抽一張牌。
背景敘述	「瞧瞧，」妃雅麗玆告訴艾拉達力：「你的天帷家園已經追隨你而來了。」
	
英文牌名	Stone Kavu
中文牌名	硬石卡甫
魔法力費用	4綠
牌的類別	生物～卡甫
畫家	Adam Rex
編號	93
力量／防禦力	3/3
規則敘述	oR：硬石卡甫得+1/+0直到回合結束。
規則敘述	oW：硬石卡甫得+0/+1直到回合結束。
背景敘述	為了保持隱蔽，牠吞噬了各種石頭——更別提各式各樣的非瑞克西亞人。
	
英文牌名	Thornscape Battlemage
中文牌名	荊景院戰巫師
魔法力費用	2綠
牌的類別	生物～魔法師
畫家	Matt Cavotta
編號	94
力量／防禦力	2/2
規則敘述	增幅oR 和／或oW
規則敘述	當荊景院戰巫師進場時，若你已支付其oR的增幅費用，則荊景院戰巫師對目標生物或玩家造成2點傷害。
規則敘述	當荊景院戰巫師進場時，若你已支付其oW的增幅費用，則消滅目標神器。
	
英文牌名	Thornscape Familiar
中文牌名	荊景院傭獸
魔法力費用	1綠
牌的類別	生物～昆蟲
畫家	Heather Hudson
編號	95
力量／防禦力	2/1
規則敘述	你所使用的紅色和白色咒語減少o1即可使用。
背景敘述	儘管有許多選擇，但荊景院大師們還是酷愛以聖甲蟲為傭獸。
	
英文牌名	Ancient Spider
中文牌名	古蜘蛛
魔法力費用	2綠白
牌的類別	生物～蜘蛛
畫家	Greg Staples
編號	96
力量／防禦力	2/5
規則敘述	先攻
規則敘述	古蜘蛛可以視同具有飛行異能地進行阻擋。
背景敘述	牠的歷史比國王或是城堡都來得久。
	
英文牌名	Cavern Harpy
中文牌名	地窟哈痞
魔法力費用	藍黑
牌的類別	生物～野獸
畫家	Daren Bader
編號	97
力量／防禦力	2/1
規則敘述	飛行
規則敘述	當地窟哈痞進場時，將一個由你操控的藍色或黑色生物移回其擁有者手上。
規則敘述	支付1點生命：將地窟哈痞移回其擁有者手上。
	
英文牌名	Cloud Cover
中文牌名	雲霧遮蔽
魔法力費用	2白藍
牌的類別	結界
畫家	Marc Fishman
編號	98
規則敘述	每當另一個由你操控的永久物成為由對手操控的咒語或異能之目標時，你可以將該永久物移回其擁有者手上。
	
英文牌名	Crosis's Charm
中文牌名	克羅希司的護符
魔法力費用	藍黑紅
牌的類別	瞬間
畫家	David Martin
編號	99
規則敘述	選擇一項～將目標永久物移回其擁有者手上；或者消滅目標非黑色的生物，且它不能重生；或者消滅目標神器。
	
英文牌名	Darigaaz's Charm
中文牌名	達里迦的護符
魔法力費用	黑紅綠
牌的類別	瞬間
畫家	David Martin
編號	100
規則敘述	選擇一項～將目標在你墳墓場中的生物牌移回你的手上；或者達里迦的護符對目標生物或玩家造成3點傷害；或者目標生物得+3/+3直到回合結束。
	
英文牌名	Daring Leap
中文牌名	放膽一躍
魔法力費用	1白藍
牌的類別	瞬間
畫家	Paolo Parente
編號	101
規則敘述	目標生物得+1/+1，並獲得飛行異能和先攻異能直到回合結束。
背景敘述	大多數的戰士僅以二次元的角度來思考。阿格內特與他的仿索藍弟兄們則早已習於三次元的戰鬥。
	
英文牌名	Destructive Flow
中文牌名	毀滅洪流
魔法力費用	黑紅綠
牌的類別	結界
畫家	Don Hazeltine
編號	102
規則敘述	在每位玩家的維持開始時，該玩家犧牲一張非基本地。
背景敘述	非瑞克西亞大侵攻的第二階段計劃，包括將多明納里亞大地重新型塑。
	
英文牌名	Doomsday Specter
中文牌名	末日幽靈
魔法力費用	2藍黑
牌的類別	生物～幽靈
畫家	Donato Giancola
編號	103
力量／防禦力	2/3
規則敘述	飛行
規則敘述	當末日幽靈進場時，將一個由你操控的藍色或黑色生物移回其擁有者手上。
規則敘述	每當末日幽靈對玩家造成戰鬥傷害時，檢視該玩家的手牌並從中選擇一張。該玩家棄掉該牌。
	
英文牌名	Dralnu's Crusade
中文牌名	卓爾努的聖戰
魔法力費用	1黑紅
牌的類別	結界
畫家	Arnie Swekel
編號	104
規則敘述	所有鬼怪得+1/+1，成為黑色，並且額外具有「僵屍」的生物類別。
背景敘述	「莫葛的智能在僵屍化之後有所進步，你不覺得嗎？」
背景敘述	～卓爾努領主
	
英文牌名	Dromar's Charm
中文牌名	德洛馬的護符
魔法力費用	白藍黑
牌的類別	瞬間
畫家	David Martin
編號	105
規則敘述	選擇一項～你獲得5點生命；或者反擊目標咒語；或者目標生物得-2/-2直到回合結束。
	
英文牌名	Eladamri's Call
中文牌名	艾拉達力的召集
魔法力費用	綠白
牌的類別	瞬間
畫家	Kev Walker
編號	106
規則敘述	從你的牌庫中搜尋一張生物牌，展示該牌，將它置入你手上。然後將你的牌庫洗牌。
背景敘述	在艾拉達力的召喚之下，部落之間的過節都暫且打住。
	
英文牌名	"Ertai, the Corrupted"
中文牌名	腐化的爾泰
魔法力費用	2白藍黑
牌的類別	生物～傳奇魔法師
畫家	Mark Tedin
編號	107
力量／防禦力	3/4
規則敘述	oU，ocT，犧牲一個生物或結界：反擊目標咒語。
背景敘述	經過非瑞克西亞科學的改造，受到黑暗魔法力的污化，甚至遭憤怒所扭曲；爾泰於鏡中倒影裡所能見證的，仍僅有自身的完美。
	
英文牌名	Fleetfoot Panther
中文牌名	疾行獵豹
魔法力費用	1綠白
牌的類別	生物～貓
畫家	Mark Brill
編號	108
力量／防禦力	3/4
規則敘述	你可以於任何你能夠使用瞬間的時機下，使用疾行獵豹。
規則敘述	當疾行獵豹進場時，將一個由你操控的綠色或白色生物移回其擁有者手上。
	
英文牌名	Gerrard's Command
中文牌名	傑拉爾德的指命
魔法力費用	綠白
牌的類別	瞬間
畫家	Roger Raupp
編號	109
規則敘述	重置目標生物。它得+3/+3直到回合結束。
背景敘述	「不想當鬼怪午餐的，馬上給我動起來！
	
英文牌名	Horned Kavu
中文牌名	刺角卡甫
魔法力費用	紅綠
牌的類別	生物～卡甫
畫家	Michael Sutfin
編號	110
力量／防禦力	3/4
規則敘述	當刺角卡甫進場時，將一個由你操控的紅色或綠色生物移回其擁有者手上。
背景敘述	「那並非自然發生，」穆塔尼道：「瑞斯的覆蓋正受到乙太的干擾。」
	
英文牌名	Hull Breach
中文牌名	艦身衝撞
魔法力費用	紅綠
牌的類別	巫術
畫家	Brian Snoddy
編號	111
規則敘述	選擇一項～消滅目標神器；或者消滅目標結界；或者消滅目標神器和目標結界。
背景敘述	「寇維克斯知道我們來了。」西賽冷笑道：「我剛才教掠奪者號撞進他的城塞。」
	
英文牌名	Keldon Twilight
中文牌名	凱爾頓的黃昏
魔法力費用	1黑紅
牌的類別	結界
畫家	Franz Vohwinkel
編號	112
規則敘述	在每位玩家回合結束時，若該回合沒有生物進行攻擊，則該玩家犧牲一個他自該回合開始便操控的生物。
	
英文牌名	Lava Zombie
中文牌名	熔岩僵屍
魔法力費用	1黑紅
牌的類別	生物～僵屍
畫家	Tom Wanerstrand
編號	113
力量／防禦力	4/3
規則敘述	當熔岩僵屍進場時，將一個由你操控的黑色或紅色生物移回其擁有者手上。
規則敘述	o2：熔岩僵屍得+1/+0直到回合結束。
	
英文牌名	Malicious Advice
中文牌名	讒言
魔法力費用	X藍黑
牌的類別	瞬間
畫家	Glen Angus
編號	114
規則敘述	橫置X個目標神器，生物，和／或地。你失去X點生命。
背景敘述	「以恐怖來支配呀，達里迦。只有讓龍心生畏懼，牠們才會忠誠地服侍你。」
背景敘述	～泰維司‧剎特
	
英文牌名	Marsh Crocodile
中文牌名	沼澤鱷魚
魔法力費用	2藍黑
牌的類別	生物～鱷魚
畫家	Kev Walker
編號	115
力量／防禦力	4/4
規則敘述	當沼澤鱷魚進場時，將一個由你操控的藍色或黑色生物移回其擁有者手上。
規則敘述	當沼澤鱷魚進場時，每位玩家各從手上棄掉一張牌。
	
英文牌名	Meddling Mage
中文牌名	擾咒法師
魔法力費用	白藍
牌的類別	生物～魔法師
畫家	Christopher Moeller
編號	116	
力量／防禦力	2/2	
規則敘述	於擾咒法師進場時，說出一張非地的牌之名稱。	
規則敘述	該名稱的牌不能使用。	
背景敘述	擾咒法師高聲吟誦，使得周圍無人能夠施術。	
		
英文牌名	Natural Emergence	
中文牌名	大自然的覺醒	
魔法力費用	2紅綠	
牌的類別	結界	
畫家	Heather Hudson	
編號	117	
規則敘述	當大自然的覺醒進場時，將由你操控的一個紅色或綠色結界移回其擁有者手上。	
規則敘述	由你操控的地成為2/2，具有先攻異能的生物。它們仍視為地。	
		
英文牌名	Phyrexian Tyranny	
中文牌名	非瑞克西亞式暴行	
魔法力費用	藍黑紅	
牌的類別	結界	
畫家	Kev Walker	
編號	118	
規則敘述	每當任一玩家抽一張牌時，除非他支付o2，否則該玩家失去2點生命。	
背景敘述	「他是約格莫夫所賜的恩典。我會每天殺他個一百多次。」	
背景敘述	～寇維克斯致爾泰	
		
英文牌名	Questing Phelddagrif	
中文牌名	遊歷的紫河馬	
魔法力費用	1綠白藍	
牌的類別	生物～紫河馬	
畫家	Matt Cavotta	
編號	119	
力量／防禦力	4/4	
規則敘述	oG：遊歷的紫河馬得+1/+1直到回合結束。目標對手將一個1/1，綠色的河馬衍生物放置進場。	
規則敘述	oW：遊歷的紫河馬獲得反黑保護及反紅保護直到回合結束。目標對手獲得2點生命。	
規則敘述	oU：遊歷的紫河馬獲得飛行異能直到回合結束。目標對手可以抽一張牌。	
		
英文牌名	Radiant Kavu	
中文牌名	眩目卡甫	
魔法力費用	紅綠白	
牌的類別	生物～卡甫	
畫家	Ron Spencer	
編號	120	
力量／防禦力	3/3	
規則敘述	oRoGoW：於本回合中，防止藍色生物和黑色生物將造成的所有戰鬥傷害。	
背景敘述	當憤怒或是受驚時，牠從身上發出眩目的強光以自衛。	
		
英文牌名	Razing Snidd	
中文牌名	夷地史尼獸	
魔法力費用	4黑紅	
牌的類別	生物～野獸	
畫家	Alan Pollack	
編號	121	
力量／防禦力	3/3	
規則敘述	當夷地史尼獸進場時，將一個由你操控的黑色或紅色生物移回其擁有者手上。	
規則敘述	當夷地史尼獸進場時，每位玩家各犧牲一張地。	
		
英文牌名	Rith's Charm	
中文牌名	麗絲的護符	
魔法力費用	紅綠白	
牌的類別	瞬間	
畫家	David Martin	
編號	122	
規則敘述	選擇一項～消滅目標非基本地；或者將三個1/1綠色腐生物衍生物放置進場；或者選擇一個來源，於本回合中，防止該來源將造成的所有傷害。	
		
英文牌名	Sawtooth Loon	
中文牌名	鋸齒潛鳥	
魔法力費用	2白藍	
牌的類別	生物～鳥	
畫家	Heather Hudson	
編號	123	
力量／防禦力	2/2	
規則敘述	飛行	
規則敘述	當鋸齒潛鳥進場時，將一個由你操控的白色或藍色生物移回其擁有者手上。	
規則敘述	當鋸齒潛鳥進場時，抽兩張牌，然後將兩張牌從你手上置於你的牌庫底。	
		
英文牌名	Shivan Wurm	
中文牌名	西瓦亞龍	
魔法力費用	3紅綠	
牌的類別	生物～亞龍	
畫家	Scott M. Fischer	
編號	124	
力量／防禦力	7/7	
規則敘述	踐踏	
規則敘述	當西瓦亞龍進場時，將一個由你操控的紅色或綠色生物移回其擁有者手上。	
		
英文牌名	Silver Drake	
中文牌名	銀色龍獸	
魔法力費用	1白藍	
牌的類別	生物～龍獸	
畫家	Alan Pollack	
編號	125	
力量／防禦力	3/3	
規則敘述	飛行	
規則敘述	當銀色龍獸進場時，將一個由你操控的白色或藍色生物移回其擁有者手上。	
		
英文牌名	Sparkcaster	
中文牌名	迸焰蜥	
魔法力費用	2紅綠	
牌的類別	生物～卡甫	
畫家	Adam Rex	
編號	126	
力量／防禦力	5/3	
規則敘述	當迸焰蜥進場時，將一個由你操控的紅色或綠色生物移回其擁有者手上。	
規則敘述	當迸焰蜥進場時，它對目標玩家造成1點傷害。	
		
英文牌名	Steel Leaf Paladin	
中文牌名	鋼葉神聖武士	
魔法力費用	4綠白	
牌的類別	生物～騎士	
畫家	Paolo Parente	
編號	127	
力量／防禦力	4/4	
規則敘述	先攻	
規則敘述	當鋼葉神聖武士進場時，將一個由你操控的綠色或白色生物移回其擁有者手上。	
		
英文牌名	Terminate	
中文牌名	終結	
魔法力費用	黑紅	
牌的類別	瞬間	
畫家	DiTerlizzi	
編號	128	
規則敘述	消滅目標生物。它不能重生。	
背景敘述	如同母親保護自己一般，達里迦獻身於保護多明納里亞，使免於擅瀆神號者之暴行。	
		
英文牌名	Treva's Charm	
中文牌名	翠瓦的護符	
魔法力費用	綠白藍	
牌的類別	瞬間	
畫家	David Martin	
編號	129	
規則敘述	選擇一項～消滅目標結界；或者將目標進行攻擊的生物移出遊戲；或者抽一張牌，然後從你手上棄掉一張牌。	
		
英文牌名	Urza's Guilt	
中文牌名	克撒之疚	
魔法力費用	2藍黑	
牌的類別	巫術	
畫家	Paolo Parente	
編號	130	
規則敘述	每位玩家抽兩張牌，然後從手上棄掉三張牌，然後失去4點生命。	
背景敘述	在約格莫夫之國度深處，克撒遽然停步，揉了揉為煤灰覆滿的雙眼：「米斯拉？」	
		
英文牌名	Draco	
中文牌名	骸龍	
魔法力費用	16	
牌的類別	神器生物～龍
畫家	Sam Wood
編號	131
力量／防禦力	9/9
規則敘述	在由你操控的地之中，每有一種基本地類別，則骸龍的費用減少o2即可使用。
規則敘述	飛行
規則敘述	在你的維持開始時，除非你支付o10，否則犧牲骸龍。在由你操控的地之中，每有一種基本地類別，則此費用便減少o2。
	
英文牌名	Mana Cylix
中文牌名	魔法力缽
魔法力費用	1
牌的類別	神器
畫家	Donato Giancola
編號	132
規則敘述	o1，ocT：加一點任意顏色的魔法力到你的魔法力池中。
背景敘述	滿溢著未來的各種面貌，這神器為活在現下的人們帶來力量。
	
英文牌名	Skyship Weatherlight
中文牌名	晴空號飛船
魔法力費用	4
牌的類別	傳奇神器
畫家	Mark Tedin
編號	133
規則敘述	當晴空號飛船進場時，從你的牌庫中搜尋任意數量的神器和／或生物牌，並將它們移出遊戲。然後將你的牌庫洗牌。
規則敘述	o4，ocT：隨機選擇一張以晴空號飛船移出遊戲的牌，並將該牌置入你手上。
	
英文牌名	Star Compass
中文牌名	繁星羅盤
魔法力費用	2
牌的類別	神器
畫家	Donato Giancola
編號	134
規則敘述	繁星羅盤須橫置進場。
規則敘述	ocT：加一點魔法力到你的魔法力池中，其顏色為由你操控的基本地能產生之任一顏色。
背景敘述	它並不指引北方。它指引著家鄉。
	
英文牌名	Stratadon
中文牌名	乳齒巨像
魔法力費用	10
牌的類別	神器生物
畫家	Brian Snoddy
編號	135
力量／防禦力	5/5
規則敘述	在由你操控的地之中，每有一種基本地類別，則乳齒巨像的費用減少o1即可使用。
規則敘述	踐踏
背景敘述	參照凱爾頓的乳齒巨象之設計：龐大而殘暴。
	
英文牌名	Crosis's Catacombs
中文牌名	克羅希司的墓穴
牌的類別	地
畫家	"Edward P. Beard, Jr."
編號	136
規則敘述	克羅希司的墓穴額外具有「巢穴」的地類別。
規則敘述	當克羅希司的墓穴進場時，除非你將一張由你操控，且非巢穴的地移回其擁有者手上，否則犧牲克羅希司的墓穴。
規則敘述	ocT：加oU，oB，或oR到你的魔法力池中。
	
英文牌名	Darigaaz's Caldera
中文牌名	達里迦的火山口
牌的類別	地
畫家	Franz Vohwinkel
編號	137
規則敘述	達里迦的火山口額外具有「巢穴」的地類別。
規則敘述	當達里迦的火山口進場時，除非你將一張由你操控，且非巢穴的地移回其擁有者手上，否則犧牲達里迦的火山口。
規則敘述	ocT：加oB，oR，或oG到你的魔法力池中。
	
英文牌名	Dromar's Cavern
中文牌名	德洛馬的地窟
牌的類別	地
畫家	Franz Vohwinkel
編號	138
規則敘述	德洛馬的地窟額外具有「巢穴」的地類別。
規則敘述	當德洛馬的地窟進場時，除非你將一張由你操控，且非巢穴的地移回其擁有者手上，否則犧牲德洛馬的地窟。
規則敘述	ocT：加oW，oU，或oB到你的魔法力池中。
	
英文牌名	Forsaken City
中文牌名	棄都
牌的類別	地
畫家	Dana Knutson
編號	139
規則敘述	棄都在你的重置步驟中不可重置。
規則敘述	在你的維持開始時，你可以將一張牌從手上移出遊戲。若你如此作，則重置棄都。
規則敘述	ocT：加一點任意顏色的魔法力到你的魔法力池中。
	
英文牌名	Meteor Crater
中文牌名	隕石坑
牌的類別	地
畫家	John Avon
編號	140
規則敘述	ocT：選擇一個由你操控的永久物具有的一個顏色。加一點該色的魔法力到你的魔法力池中。
背景敘述	根據傳說，因隕石撞擊而喪生的鬼魂常在隕石坑附近作祟。
	
英文牌名	Rith's Grove
中文牌名	麗絲的叢林
牌的類別	地
畫家	Scott Bailey
編號	141
規則敘述	麗絲的叢林額外具有「巢穴」的地類別。
規則敘述	當麗絲的叢林進場時，除非你將一張由你操控，且非巢穴的地移回其擁有者手上，否則犧牲麗絲的叢林。
規則敘述	ocT：加oR，oG，或oW到你的魔法力池中。
	
英文牌名	Terminal Moraine
中文牌名	冰磧岩緣
牌的類別	地
畫家	Scott Bailey
編號	142
規則敘述	ocT：加一點無色魔法力到你的魔法力池中。
規則敘述	o2，ocT，犧牲冰磧岩緣：從你的牌庫中搜尋一張基本地牌，並將該牌橫置進場。然後將你的牌庫洗牌。
	
英文牌名	Treva's Ruins
中文牌名	翠瓦的遺跡
牌的類別	地
畫家	Jerry Tiritilli
編號	143
規則敘述	翠瓦的遺跡額外具有「巢穴」的地類別。
規則敘述	當翠瓦的遺跡進場時，除非你將一張由你操控，且非巢穴的地移回其擁有者手上，否則犧牲翠瓦的遺跡。
規則敘述	ocT：加oG，oW，或oU到你的魔法力池中。
	
英文牌名	"Ertai, the Corrupted"
中文牌名	腐化的爾泰
魔法力費用	2白藍黑
牌的類別	生物～傳奇魔法師
畫家	Kev Walker
編號	*107
力量／防禦力	3/4
規則敘述	oU，ocT，犧牲一個生物或結界：反擊目標咒語。
背景敘述	經過非瑞克西亞科學的改造，受到黑暗魔法力的污化，甚至遭憤怒所扭曲；爾泰於鏡中倒影裡所能見證的，仍僅有自身的完美。
	
英文牌名	Skyship Weatherlight
中文牌名	晴空號飛船
魔法力費用	4
牌的類別	傳奇神器
畫家	Kev Walker
編號	*133
規則敘述	當晴空號飛船進場時，從你的牌庫中搜尋任意數量的神器和／或生物牌，並將它們移出遊戲。然後將你的牌庫洗牌。
規則敘述	o4，ocT：隨機選擇一張以晴空號飛船移出遊戲的牌，並將該牌置入你手上。
	
英文牌名	"Tahngarth, Talruum Hero"
中文牌名	塔路姆英雄坦格爾斯
魔法力費用	3紅紅
牌的類別	生物～傳奇牛頭怪
畫家	Kev Walker
編號	*74
力量／防禦力	4/4
規則敘述	塔路姆英雄坦格爾斯攻擊時不須橫置。
規則敘述	o1oR，ocT：坦格爾斯對目標生物造成等同於坦格爾斯力量的傷害。該生物對坦格爾斯造成等同於該生物力量的傷害。
	
