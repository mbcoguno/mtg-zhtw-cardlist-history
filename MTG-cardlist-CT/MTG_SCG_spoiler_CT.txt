劫運降臨完整卡表

顏色／稀有度	白色／稀有
英文牌名	Ageless Sentinels
中文牌名	長生哨兵
魔法力費用	{三}{白}
類別	生物～牆
收集編號	1/143
力量／防禦力	4/4
規則敘述	（牆不能攻擊）
規則敘述	飛行
規則敘述	當長生哨兵進行阻擋時，其生物類別成為巨人／鳥。（它不再是牆。此效應不因回合結束而終止。）
	
顏色／稀有度	白色／普通
英文牌名	Astral Steel
中文牌名	星幽鋼刃
魔法力費用	{二}{白}
類別	瞬間
收集編號	2/143
規則敘述	目標生物得+1/+2直到回合結束。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。你可以為複製品選擇新的目標。）
	
顏色／稀有度	白色／普通
英文牌名	Aven Farseer
中文牌名	艾文預言師
魔法力費用	{一}{白}
類別	生物～鳥／士兵
收集編號	3/143
力量／防禦力	1/1
規則敘述	飛行
規則敘述	每當一個生物翻回正面時，在艾文預言師上放置一個+1/+1指示物。
背景敘述	敵人的奸滑無恥，只使他愈加義憤。
	
顏色／稀有度	白色／普通
英文牌名	Aven Liberator
中文牌名	艾文解放者
魔法力費用	{二}{白}{白}
類別	生物～鳥／士兵
收集編號	4/143
力量／防禦力	2/3
規則敘述	飛行
規則敘述	變身{三}{白}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當艾文解放者翻回正面時，選擇一種顏色，目標由你操控的生物獲得反該色保護直到回合結束。
	
顏色／稀有度	白色／普通
英文牌名	Daru Spiritualist
中文牌名	韃魯靈學師
魔法力費用	{一}{白}
類別	生物～僧侶
收集編號	5/143
力量／防禦力	1/1
規則敘述	每當任一由你操控的僧侶成為咒語或異能的目標時，該僧侶得+0/+2直到回合結束。
背景敘述	他提高族人精神，以期對敵降下怒火。
	
顏色／稀有度	白色／非普通
英文牌名	Daru Warchief
中文牌名	韃魯戰酋長
魔法力費用	{二}{白}{白}
類別	生物～士兵
收集編號	6/143
力量／防禦力	1/1
規則敘述	你使用的士兵咒語費用減少{一}來使用。
規則敘述	由你操控的士兵得+1/+2。
背景敘述	勝利替他說明了一切。
	
顏色／稀有度	白色／稀有
英文牌名	Dawn Elemental
中文牌名	黎明元素
魔法力費用	{白}{白}{白}{白}
類別	生物～元素
收集編號	7/143
力量／防禦力	3/3
規則敘述	飛行
規則敘述	防止將對黎明元素造成的所有傷害。
背景敘述	方其時，韃魯平原夜明如旭。
	
顏色／稀有度	白色／稀有
英文牌名	Decree of Justice
中文牌名	正義的宣判
魔法力費用	{X}{X}{二}{白}{白}
類別	巫術
收集編號	8/143
規則敘述	將X個4/4白色，具飛行異能的天使衍生物放置進場。
規則敘述	循環{二}{白}
規則敘述	當你循環正義的宣判時，你可以支付{X}。若你如此作，則放置X個1/1白色士兵衍生物進場。
	
顏色／稀有度	白色／稀有
英文牌名	Dimensional Breach
中文牌名	次元裂罅
魔法力費用	{五}{白}{白}
類別	巫術
收集編號	9/143
規則敘述	將所有永久物移出遊戲。只要還有任何因此法移出遊戲的牌在遊戲外，則在每位玩家的維持開始時，他將其中一張由他擁有的牌移回場上。
	
顏色／稀有度	白色／普通
英文牌名	Dragon Scales
中文牌名	龍鱗
魔法力費用	{一}{白}
類別	生物結界
收集編號	10/143
規則敘述	受此結界的生物得+1/+2且攻擊時不需橫置。
規則敘述	當總魔法力費用大於或等於6的生物進場時，你可以將龍鱗從你的墳墓場中移回場上，並結附於該生物上。
	
顏色／稀有度	白色／非普通
英文牌名	Dragonstalker
中文牌名	降龍使
魔法力費用	{四}{白}
類別	生物～鳥／士兵
收集編號	11/143
力量／防禦力	3/3
規則敘述	飛行，反龍保護
背景敘述	「天火蔽空之時，勇者將挺身而出。」
背景敘述	～教團先知
	
顏色／稀有度	白色／稀有
英文牌名	Eternal Dragon
中文牌名	不朽巨龍
魔法力費用	{五}{白}{白}
類別	生物～龍／精靈
收集編號	12/143
力量／防禦力	5/5
規則敘述	飛行
規則敘述	{三}{白}{白}：將不朽巨龍從你的墳墓場移回你的手上。你只能於你的維持中使用此異能。
規則敘述	循環平原{二}（{二}，從你手上棄掉此牌：從你的牌庫中搜尋一張平原牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。）
	
顏色／稀有度	白色／稀有
英文牌名	Exiled Doomsayer
中文牌名	受逐的劫難論者
魔法力費用	{一}{白}
類別	生物～僧侶
收集編號	13/143
力量／防禦力	1/2
規則敘述	所有變身費用須多支付{二}。（以牌面朝下方式使用生物牌的費用並不受影響。）
背景敘述	他盡力遏止已預見的未來。
	
顏色／稀有度	白色／稀有
英文牌名	Force Bubble
中文牌名	氣泡護罩
魔法力費用	{二}{白}{白}
類別	結界
收集編號	14/143
規則敘述	若你將受到傷害，則改為在氣泡護罩上放置等量的消耗指示物。
規則敘述	當氣泡護罩上有四個或更多消耗指示物時，將其犧牲。
規則敘述	在回合結束時，從氣泡護罩上移去所有消耗指示物。
	
顏色／稀有度	白色／普通
英文牌名	Frontline Strategist
中文牌名	前線策士
魔法力費用	{白}
類別	生物～士兵
收集編號	15/143
力量／防禦力	1/1
規則敘述	變身{白}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當前線策士翻回正面時，防止本回合中非士兵所造成的戰鬥傷害。
	
顏色／稀有度	白色／非普通
英文牌名	Gilded Light
中文牌名	金光眩目
魔法力費用	{一}{白}
類別	瞬間
收集編號	16/143
規則敘述	你本回合中不能成為咒語或異能的目標。
規則敘述	循環{二}（{二}，從你手上棄掉此牌：抽一張牌。）
背景敘述	「撐過第一擊才能還擊。」
	
顏色／稀有度	白色／普通
英文牌名	Guilty Conscience
中文牌名	罪惡感
魔法力費用	{白}
類別	生物結界
收集編號	17/143
規則敘述	每當受此結界的生物造成傷害時，罪惡感對受此結界的生物造成等量的傷害。
背景敘述	感到內疚的人通常不需如此，而應該內疚的人通常從不如此。
背景敘述	～教團格言
	
顏色／稀有度	白色／非普通
英文牌名	Karona's Zealot
中文牌名	卡若娜狂信者
魔法力費用	{四}{白}
類別	生物～僧侶
收集編號	18/143
力量／防禦力	2/5
規則敘述	變身{三}{白}{白}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當卡若娜狂信者翻回正面時，本回合所有將對其造成的傷害改為對目標生物造成之。
	
顏色／稀有度	白色／普通
英文牌名	Noble Templar
中文牌名	尊貴聖殿騎士
魔法力費用	{五}{白}
類別	生物～僧侶／士兵
收集編號	19/143
力量／防禦力	3/6
規則敘述	尊貴聖殿騎士攻擊時不需橫置。
規則敘述	循環平原{二}（{二}，從你手上棄掉此牌：從你的牌庫中搜尋一張平原牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。）
	
顏色／稀有度	白色／非普通
英文牌名	Rain of Blades
中文牌名	劍雨
魔法力費用	{白}
類別	瞬間
收集編號	20/143
規則敘述	劍雨對每個進行攻擊的生物各造成1點傷害。
背景敘述	據說它們是殉戰英雄的武器，急欲尋覓最後一次的榮耀。
顏色／稀有度	白色／普通
英文牌名	Recuperate
中文牌名	恢復健康
魔法力費用	{三}{白}
類別	瞬間
收集編號	21/143
規則敘述	選擇一項～你獲得6點生命；或防止本回合中接下來將對目標生物造成的6點傷害。
背景敘述	「起身吧，你還得繼續殺敵。」
背景敘述	～偽神卡若娜
	
顏色／稀有度	白色／非普通
英文牌名	Reward the Faithful
中文牌名	獎賞忠誠
魔法力費用	{白}
類別	瞬間
收集編號	22/143
規則敘述	任意數量的目標玩家各獲得生命，其數量等同於由你操控的永久物間最高之總魔法力費用。
背景敘述	「當你飲足後，將杯子傳給其他還渴著的人。」
	
顏色／稀有度	白色／非普通
英文牌名	Silver Knight
中文牌名	銀騎士
魔法力費用	{白}{白}
類別	生物～騎士
收集編號	23/143
力量／防禦力	2/2
規則敘述	先攻，反紅保護
背景敘述	在張狂的混沌浪潮下，他是歐塔利亞的最後防線。
	
顏色／稀有度	白色／稀有
英文牌名	Trap Digger
中文牌名	佈陷人
魔法力費用	{三}{白}
類別	生物～士兵
收集編號	24/143
力量／防禦力	1/3
規則敘述	{二}{白}，{橫置}：在目標由你操控的地上放置一個陷阱指示物。
規則敘述	犧牲一個上面有陷阱指示物的地：佈陷人對目標進行攻擊，且不具飛行異能的生物造成3點傷害。
	
顏色／稀有度	白色／非普通
英文牌名	Wing Shards
中文牌名	翎羽飛射
魔法力費用	{一}{白}{白}
類別	瞬間
收集編號	25/143
規則敘述	目標玩家犧牲一個進行攻擊的生物。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。）
	
顏色／稀有度	白色／普通
英文牌名	Wipe Clean
中文牌名	掃淨
魔法力費用	{一}{白}
類別	瞬間
收集編號	26/143
規則敘述	將目標結界移出遊戲。
規則敘述	循環{三}（{三}，從你手上棄掉此牌：抽一張牌。）
背景敘述	「祖靈聖光將盡掃大地幽穢。」
	
顏色／稀有度	白色／普通
英文牌名	Zealous Inquisitor
中文牌名	狂熱審判官
魔法力費用	{二}{白}
類別	生物～僧侶
收集編號	27/143
力量／防禦力	2/2
規則敘述	{一}{白}：於本回合中，狂熱審判官將受到的下1點傷害改為對目標生物造成之。
背景敘述	「我僅是將屬於他人的歸還原主。」
	
顏色／稀有度	藍色／非普通
英文牌名	Aphetto Runecaster
中文牌名	艾非托符文使士
魔法力費用	{三}{藍}
類別	生物～魔法師
收集編號	28/143
力量／防禦力	2/3
規則敘述	每當一個生物翻回正面時，你可以抽一張牌。
背景敘述	「愚者畏懼改變，智者則將之化為武器。」
	
顏色／稀有度	藍色／非普通
英文牌名	Brain Freeze
中文牌名	腦凍結
魔法力費用	{一}{藍}
類別	瞬間
收集編號	29/143
規則敘述	目標玩家將他牌庫頂的三張牌置入其墳墓場。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。你可以為複製品選擇新的目標。）
	
顏色／稀有度	藍色／普通
英文牌名	Coast Watcher
中文牌名	海岸看守人
魔法力費用	{一}{藍}
類別	生物～鳥／士兵
收集編號	30/143
力量／防禦力	1/1
規則敘述	飛行，反綠保護
背景敘述	艾文開始畏懼張牙舞爪的林枝，但看守人迅速重燃他們的勇氣。
	
顏色／稀有度	藍色／稀有
英文牌名	Day of the Dragons
中文牌名	諸龍時光
魔法力費用	{四}{藍}{藍}{藍}
類別	結界
收集編號	31/143
規則敘述	當諸龍時光進場時，將所有由你操控的生物移出遊戲。然後將相同數量之5/5紅色，具飛行異能的龍衍生物放置進場。
規則敘述	當諸龍時光離場時，犧牲所有由你操控的龍。然後將以此法移出遊戲的牌在你的操控下放置進場。
	
顏色／稀有度	藍色／稀有
英文牌名	Decree of Silence
中文牌名	寂靜的宣判
魔法力費用	{六}{藍}{藍}
類別	結界
收集編號	32/143
規則敘述	每當對手使用咒語時，將其反擊並在寂靜的宣判上放置一個消耗指示物。若寂靜的宣判上有三個或更多消耗指示物，則將其消滅。
規則敘述	循環{四}{藍}{藍}
規則敘述	當你循環寂靜的宣判時，你可以反擊目標咒語。
	
顏色／稀有度	藍色／普通
英文牌名	Dispersal Shield
中文牌名	離散護罩
魔法力費用	{一}{藍}
類別	瞬間
收集編號	33/143
規則敘述	計算由你操控的永久物間最高之總魔法力費用，若目標咒語之總魔法力費用等於或小於該數，則將其反擊。
背景敘述	「下次再來吧。」
	
顏色／稀有度	藍色／普通
英文牌名	Dragon Wings
中文牌名	龍翼
魔法力費用	{一}{藍}
類別	生物結界
收集編號	34/143
規則敘述	受此結界的生物具有飛行異能。
規則敘述	循環{一}{藍}（{一}{藍}，從你手上棄掉此牌：抽一張牌。）
規則敘述	當總魔法力費用大於或等於6的生物進場時，你可以將龍翼從你的墳墓場中移回場上，並結附於該生物上。
	
顏色／稀有度	藍色／稀有
英文牌名	Faces of the Past
中文牌名	過去的面貌
魔法力費用	{二}{藍}
類別	結界
收集編號	35/143
規則敘述	每當一個生物從場上進入墳墓場時，橫置或重置所有與其有共通生物類別的生物。
背景敘述	用來綁縛的繩結也能拿來窒息。
	
顏色／稀有度	藍色／普通
英文牌名	Frozen Solid
中文牌名	冰封
魔法力費用	{一}{藍}{藍}
類別	生物結界
收集編號	36/143
規則敘述	受此結界的生物於其操控者的重置步驟中不可重置。
規則敘述	當受此結界的生物受到傷害時，將其消滅。
	
顏色／稀有度	藍色／普通
英文牌名	Hindering Touch
中文牌名	阻礙之觸
魔法力費用	{三}{藍}
類別	瞬間
收集編號	37/143
規則敘述	除非目標咒語的操控者支付{二}，否則反擊之。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。你可以為複製品選擇新的目標。）
	
顏色／稀有度	藍色／非普通
英文牌名	Long-Term Plans
中文牌名	長期計劃
魔法力費用	{二}{藍}
類別	瞬間
收集編號	38/143
規則敘述	從你的牌庫中搜尋一張牌，將你的牌庫洗牌，然後將該牌置於牌庫頂數來第三張的位置。
背景敘述	「等等，我就快想到了。」
	
顏色／稀有度	藍色／普通
英文牌名	Mercurial Kite
中文牌名	水銀巧鳶
魔法力費用	{三}{藍}
類別	生物～鳥
收集編號	39/143
力量／防禦力	2/2
規則敘述	飛行
規則敘述	每當水銀巧鳶對生物造成戰鬥傷害時，橫置該生物。該生物於其操控者的下一個重置步驟中不可重置。
	
顏色／稀有度	藍色／非普通
英文牌名	Metamorphose
中文牌名	徹底改變
魔法力費用	{一}{藍}
類別	瞬間
收集編號	40/143
規則敘述	將目標由對手操控的永久物置於其擁有者的牌庫頂。該對手可從其手上將一個神器，生物，結界，或地牌放置進場。
背景敘述	如果不要這個，就換下一個。
	
顏色／稀有度	藍色／稀有
英文牌名	Mind's Desire
中文牌名	心之所欲
魔法力費用	{四}{藍}{藍}
類別	巫術
收集編號	41/143
規則敘述	將你的牌庫洗牌。然後將你的牌庫頂牌移出遊戲。直到回合結束，你可將它視為在你手上一般地使用，且不須支付其魔法力費用。（若該牌之魔法力費用中含有Ｘ，則Ｘ為0。）
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。）
	
顏色／稀有度	藍色／稀有
英文牌名	Mischievous Quanar
中文牌名	惱人魁拿獸
魔法力費用	{四}{藍}
類別	生物～野獸
收集編號	42/143
力量／防禦力	3/3
規則敘述	{三}{藍}{藍}：將惱人魁拿獸的牌面朝下。
規則敘述	變身{一}{藍}{藍}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當惱人魁拿獸翻回正面時，複製目標瞬間或巫術咒語。你可以為該複製選擇新的目標。
	
顏色／稀有度	藍色／非普通
英文牌名	Mistform Warchief
中文牌名	霧幻戰酋長
魔法力費用	{二}{藍}
類別	生物～虛影
收集編號	43/143
力量／防禦力	1/3
規則敘述	由你使用之生物咒語，且與霧幻戰酋長有共通的生物類別者，其費用減少{一}來使用。
規則敘述	{橫置}：選擇一種生物類別，霧幻戰酋長成為該類別直到回合結束。
	
顏色／稀有度	藍色／稀有
英文牌名	Parallel Thoughts
中文牌名	平行思緒
魔法力費用	{三}{藍}{藍}
類別	結界
收集編號	44/143
規則敘述	當平行思緒進場時，從你的牌庫中搜尋七張牌，將這些牌移出遊戲，牌面朝下地放成一堆，再將該堆洗牌。然後將你的牌庫洗牌。
規則敘述	若你將抽一張牌，則你可以改為將該堆的頂牌置於你手上。
	
顏色／稀有度	藍色／非普通
英文牌名	Pemmin's Aura
中文牌名	潘明的靈氣
魔法力費用	{一}{藍}{藍}
類別	生物結界
收集編號	45/143
規則敘述	{藍}：重置受此結界的生物。
規則敘述	{藍}：受此結界的生物獲得飛行異能直到回合結束。
規則敘述	{藍}：受此結界的生物本回合不能成為咒語或異能的目標。
規則敘述	{一}：受此結界的生物得+1/-1或-1/+1直到回合結束。
	
顏色／稀有度	藍色／普通
英文牌名	Raven Guild Initiate
中文牌名	墨鴉公會新手
魔法力費用	{二}{藍}
類別	生物～魔法師
收集編號	46/143
力量／防禦力	1/4
規則敘述	變身～將一個由你操控的鳥移回其擁有者手上。（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
背景敘述	墨鴉公會滑翔於不安之風中。
	
顏色／稀有度	藍色／稀有
英文牌名	Raven Guild Master
中文牌名	墨鴉公會師傅
魔法力費用	{一}{藍}{藍}
類別	生物～魔法師／突變體
收集編號	47/143
力量／防禦力	1/1
規則敘述	每當墨鴉公會師傅對玩家造成戰鬥傷害時，該玩家將他牌庫頂上的十張牌移出遊戲。
規則敘述	變身{二}{藍}{藍}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
	
顏色／稀有度	藍色／非普通
英文牌名	Riptide Survivor
中文牌名	激流餘生者
魔法力費用	{二}{藍}
類別	生物～魔法師
收集編號	48/143
力量／防禦力	2/1
規則敘述	變身{一}{藍}{藍}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當激流餘生者翻回正面時，從你手上棄掉兩張牌，然後抽三張牌。
	
顏色／稀有度	藍色／普通
英文牌名	Rush of Knowledge
中文牌名	知識湧昇
魔法力費用	{四}{藍}
類別	巫術
收集編號	49/143
規則敘述	抽若干牌，其數量等同於由你操控的永久物間最高之總魔法力費用。
背景敘述	「求得無窮的悟解之後，無窮的力量便不再引人。」
背景敘述	～實相塑師意悉多
	
顏色／稀有度	藍色／普通
英文牌名	Scornful Egotist
中文牌名	狂傲客
魔法力費用	{七}{藍}
類別	生物～魔法師
收集編號	50/143
力量／防禦力	1/1
規則敘述	變身{藍}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
背景敘述	「我曾是人類。現在的我更加超越。」
	
顏色／稀有度	藍色／普通
英文牌名	Shoreline Ranger
中文牌名	沿岸巡邏人
魔法力費用	{五}{藍}
類別	生物～鳥／士兵
收集編號	51/143
力量／防禦力	3/4
規則敘述	飛行
規則敘述	循環海島{二}（{二}，從你手上棄掉此牌：從你的牌庫中搜尋一張海島牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。）
	
顏色／稀有度	藍色／稀有
英文牌名	Stifle
中文牌名	阻抑
魔法力費用	{藍}
類別	瞬間
收集編號	52/143
規則敘述	反擊目標起動式或觸發式異能。（魔法力異能不能被反擊）
背景敘述	「如果我要問你的意見，相信你剛才已經聽到了。」
背景敘述	～激流餘生者潘明
	
顏色／稀有度	藍色／普通
英文牌名	Temporal Fissure
中文牌名	暫現隙口
魔法力費用	{四}{藍}
類別	巫術
收集編號	53/143
規則敘述	將目標永久物移回其擁有者的手上。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。你可以為複製品選擇新的目標。）
	
顏色／稀有度	藍色／非普通
英文牌名	Thundercloud Elemental
中文牌名	雷雲元素
魔法力費用	{五}{藍}{藍}
類別	生物～元素
收集編號	54/143
力量／防禦力	3/4
規則敘述	飛行
規則敘述	{三}{藍}：橫置所有防禦力等於或小於2的生物。
規則敘述	{三}{藍}：所有其他生物失去飛行異能直到回合結束。
背景敘述	有些日子最好別出門。
	
顏色／稀有度	黑色／非普通
英文牌名	Bladewing's Thrall
中文牌名	鋒翼屍奴
魔法力費用	{二}{黑}{黑}
類別	生物～殭屍
收集編號	55/143
力量／防禦力	3/3
規則敘述	只要你操控龍，鋒翼屍奴便具有飛行異能。
規則敘述	當一個龍進場時，你可以將鋒翼屍奴從你的墳墓場中移回場上。
	
顏色／稀有度	黑色／稀有
英文牌名	Cabal Conditioning
中文牌名	柯幫式制約
魔法力費用	{六}{黑}
類別	巫術
收集編號	56/143
規則敘述	任意數量的目標玩家各棄若干牌，其數量等同於由你操控的永久物間最高之總魔法力費用。
背景敘述	「柯幫之聲唯一。
背景敘述	柯幫之道唯一。
背景敘述	柯幫之令唯一。」
背景敘述	～柯幫禱文
	
顏色／稀有度	黑色／非普通
英文牌名	Cabal Interrogator
中文牌名	柯幫詰問人
魔法力費用	{一}{黑}
類別	生物～殭屍／魔法師
收集編號	57/143
力量／防禦力	1/1
規則敘述	{X}{黑}，{橫置}：目標玩家展示其手上的X張牌並且你從中選擇一張。該玩家棄掉該牌。你只可以於你能使用巫術的時機下使用此異能。
	
顏色／稀有度	黑色／稀有
英文牌名	Call to the Grave
中文牌名	邀眾入土
魔法力費用	{四}{黑}
類別	結界
收集編號	58/143
規則敘述	在每位玩家的維持開始時，該玩家犧牲一個非殭屍的生物。
規則敘述	在回合結束時，若場上沒有生物，則犧牲邀眾入土。
	
顏色／稀有度	黑色／普通
英文牌名	Carrion Feeder
中文牌名	腐食怪
魔法力費用	{黑}
類別	生物～殭屍
收集編號	59/143
力量／防禦力	1/1
規則敘述	腐食怪不能進行阻擋。
規則敘述	犧牲一個生物：在腐食怪上放置一個+1/+1指示物。
背景敘述	他帶著腐敗氣息在墓碑間跳躍，尋找著下一餐。
	
顏色／稀有度	黑色／非普通
英文牌名	Chill Haunting
中文牌名	寒冰迫魂
魔法力費用	{一}{黑}
類別	瞬間
收集編號	60/143
規則敘述	從你墳墓場中將X張生物牌移出遊戲，以作為使用寒冰迫魂的額外費用。
規則敘述	目標生物得-X/-X直到回合結束。
	
顏色／稀有度	黑色／普通
英文牌名	Clutch of Undeath
中文牌名	亡物魔掌
魔法力費用	{三}{黑}{黑}
類別	生物結界
收集編號	61/143
規則敘述	受此結界的生物若為殭屍，則它得+3/+3；否則它得-3/-3。
背景敘述	亡物之手會辨別死活。
	
顏色／稀有度	黑色／稀有
英文牌名	Consumptive Goo
中文牌名	吞噬流漿
魔法力費用	{黑}{黑}
類別	生物～流漿
收集編號	62/143
力量／防禦力	1/1
規則敘述	{二}{黑}{黑}：目標生物得-1/-1直到回合結束。在吞噬流漿上放置一個+1/+1指示物。
背景敘述	霧般幽靜，惡疫般無情；它是潮濕、蠕動的死亡。
	
顏色／稀有度	黑色／普通
英文牌名	Death's-Head Buzzard
中文牌名	骷髏頭兀鷹
魔法力費用	{一}{黑}{黑}
類別	生物～鳥
收集編號	63/143
力量／防禦力	2/1
規則敘述	飛行
規則敘述	當骷髏頭兀鷹從場上進入墳墓場時，所有生物得-1/-1直到回合結束。
背景敘述	滿身疫疾，從不飽足，自夜空掠下。
	
顏色／稀有度	黑色／稀有
英文牌名	Decree of Pain
中文牌名	苦痛的宣判
魔法力費用	{六}{黑}{黑}
類別	巫術
收集編號	64/143
規則敘述	消滅所有生物。它們不能重生。每以此法消滅一個生物，便抽一張牌。
規則敘述	循環{三}{黑}{黑}
規則敘述	當你循環苦痛的宣判時，所有生物得-2/-2直到回合結束。
	
顏色／稀有度	黑色／普通
英文牌名	Dragon Shadow
中文牌名	龍影
魔法力費用	{一}{黑}
類別	生物結界
收集編號	65/143
規則敘述	受此結界的生物得+1/+0並具有恐懼異能。（此生物只能被神器和／或黑色生物阻擋。）
規則敘述	當總魔法力費用大於或等於6的生物進場時，你可以將龍影從你的墳墓場中移回場上，並結附於該生物上。
	
顏色／稀有度	黑色／非普通
英文牌名	Fatal Mutation
中文牌名	致命變異
魔法力費用	{黑}
類別	生物結界
收集編號	66/143
規則敘述	當受此結界的生物翻回正面時，將其消滅。它不能重生。
背景敘述	「外殼曾是你的面具；現在它是你的棺材。」
背景敘述	～柯幫僧侶
	
顏色／稀有度	黑色／稀有
英文牌名	Final Punishment
中文牌名	終極刑罰
魔法力費用	{三}{黑}{黑}
類別	巫術
收集編號	67/143
規則敘述	目標玩家失去生命，其數量等同於他本回合已受的傷害。
背景敘述	一生中的痛苦～每次刮傷，病痛，挫傷～濃縮於一刻。
	
顏色／稀有度	黑色／稀有
英文牌名	Lethal Vapors
中文牌名	致命氣息
魔法力費用	{二}{黑}{黑}
類別	結界
收集編號	68/143
規則敘述	每當任一生物進場時，將其消滅。
規則敘述	{零}：消滅致命氣息。你略過你的下一個回合。任何玩家都可使用此異能。
背景敘述	此氣息滲過每個裂縫，毒害每個心肺，消滅每個活物。
	
顏色／稀有度	黑色／普通
英文牌名	Lingering Death
中文牌名	厄影徘徊
魔法力費用	{一}{黑}
類別	生物結界
收集編號	69/143
規則敘述	受此結界的生物之操控者在其回合結束將其犧牲。
背景敘述	「狀況不妙。我不知道他是否能撐過今晚。」
背景敘述	～柯幫僧侶
	
顏色／稀有度	黑色／稀有
英文牌名	Nefashu
中文牌名	惡法殊
魔法力費用	{四}{黑}{黑}
類別	生物～殭屍／突變體
收集編號	70/143
力量／防禦力	5/3
規則敘述	每當惡法殊攻擊時，至多五個目標生物各得-1/-1直到回合結束。
背景敘述	惡法殊所經之處，鮮血如絲毯舖滿大地。
	
顏色／稀有度	黑色／非普通
英文牌名	Putrid Raptor
中文牌名	腐臭迅猛龍
魔法力費用	{四}{黑}{黑}
類別	生物～殭屍／野獸
收集編號	71/143
力量／防禦力	4/4
規則敘述	變身～從你手上棄掉一張殭屍牌。（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
背景敘述	它急忙吞嚥著從自己身上落入沼澤地的腐肉。
	
顏色／稀有度	黑色／普通
英文牌名	Reaping the Graves
中文牌名	墳場收成
魔法力費用	{二}{黑}
類別	瞬間
收集編號	72/143
規則敘述	將目標在你墳墓場中的生物牌移回你的手上。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。你可以為複製品選擇新的目標。）
	
顏色／稀有度	黑色／普通
英文牌名	Skulltap
中文牌名	開顱術
魔法力費用	{一}{黑}
類別	巫術
收集編號	73/143
規則敘述	犧牲一個生物，以作為使用開顱術的額外費用。
規則敘述	抽兩張牌。
背景敘述	「你的腦袋總算派上用場了。」
背景敘述	～懼像召喚師布蕾德
	
顏色／稀有度	黑色／稀有
英文牌名	Soul Collector
中文牌名	蒐魂妖
魔法力費用	{三}{黑}{黑}
類別	生物～吸血鬼
收集編號	74/143
力量／防禦力	3/4
規則敘述	飛行
規則敘述	每當本回合中曾受到蒐魂妖傷害的生物置入墳墓場時，將該牌在你的操控下移回場上。
規則敘述	變身{黑}{黑}{黑}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
	
顏色／稀有度	黑色／非普通
英文牌名	Tendrils of Agony
中文牌名	苦痛捲鬚
魔法力費用	{二}{黑}{黑}
類別	巫術
收集編號	75/143
規則敘述	目標玩家失去2點生命，並且你獲得2點生命。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。你可以為複製品選擇新的目標。）
	
顏色／稀有度	黑色／普通
英文牌名	Twisted Abomination
中文牌名	曲惡憎恨獸
魔法力費用	{五}{黑}
類別	生物～殭屍／突變體
收集編號	76/143
力量／防禦力	5/3
規則敘述	{黑}：重生曲惡憎恨獸。
規則敘述	循環沼澤{二}（{二}，從你手上棄掉此牌：從你的牌庫中搜尋一張沼澤牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。）
	
顏色／稀有度	黑色／普通
英文牌名	Unburden
中文牌名	忘卻煩惱
魔法力費用	{一}{黑}{黑}
類別	巫術
收集編號	77/143
規則敘述	目標玩家棄掉兩張牌。
規則敘述	循環{二}（{二}，從你手上棄掉此牌：抽一張牌。）
背景敘述	柯幫學徒入學時飽含希望與畏懼之情；畢業時兩者都消失無蹤。
	
顏色／稀有度	黑色／非普通
英文牌名	Undead Warchief
中文牌名	不死戰酋長
魔法力費用	{二}{黑}{黑}
類別	生物～殭屍
收集編號	78/143
力量／防禦力	1/1
規則敘述	你使用的殭屍咒語費用減少{一}來使用。
規則敘述	由你操控的殭屍得+2/+1。
背景敘述	它的蠻力可抵七人。其實，它之前本來就是七個人。
	
顏色／稀有度	黑色／非普通
英文牌名	Unspeakable Symbol
中文牌名	禁忌徽記
魔法力費用	{一}{黑}{黑}
類別	結界
收集編號	79/143
規則敘述	支付3點生命：在目標生物上放置一個+1/+1指示物。
背景敘述	此徽記遍佈艾非托，指引墨鴉公會與柯幫的下僕們安身之地。
	
顏色／稀有度	黑色／普通
英文牌名	Vengeful Dead
中文牌名	復仇亡者
魔法力費用	{三}{黑}
類別	生物～殭屍
收集編號	80/143
力量／防禦力	3/2
規則敘述	每當復仇亡者或另一個殭屍從場上進入墳墓場時，每位對手各失去1點生命。
背景敘述	不能從死亡中學得教訓者註定要重蹈覆轍。
	
顏色／稀有度	黑色／普通
英文牌名	Zombie Cutthroat 
中文牌名	殭屍割喉者
魔法力費用	{三}{黑}{黑}
類別	生物～殭屍
收集編號	81/143
力量／防禦力	3/4
規則敘述	變身～支付5點生命。（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
背景敘述	如殭屍般心無二用，同殺手般詭計多端。
	
顏色／稀有度	紅色／普通
英文牌名	Bonethorn Valesk
中文牌名	骨棘伐雷獸
魔法力費用	{四}{紅}
類別	生物～野獸
收集編號	82/143
力量／防禦力	4/2
規則敘述	每當一個生物翻回正面時，骨棘伐雷獸對目標生物或玩家造成1點傷害。
背景敘述	野蠻人將其骨棘編織成祭典飾物，以證武勇。
	
顏色／稀有度	紅色／非普通
英文牌名	Carbonize
中文牌名	碳化
魔法力費用	{二}{紅}
類別	瞬間
收集編號	83/143
規則敘述	碳化對目標生物或玩家造成3點傷害。該生物於本回合中不能重生。若它於本回合中將被置入墳墓場，則改為將其移出遊戲。
	
顏色／稀有度	紅色／普通
英文牌名	Chartooth Cougar
中文牌名	烏牙山獅
魔法力費用	{五}{紅}
類別	生物～貓／野獸
收集編號	84/143
力量／防禦力	4/4
規則敘述	{紅}：烏牙山獅得+1/+0直到回合結束。
規則敘述	山脈循環{二}（{二}，從你手上棄掉此牌：從你的牌庫中搜尋一張山脈牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。）
	
顏色／稀有度	紅色／稀有
英文牌名	Decree of Annihilation
中文牌名	滅絕的宣判
魔法力費用	{八}{紅}{紅}
類別	巫術
收集編號	85/143
規則敘述	將所有神器，生物，地，墳墓場，以及手牌移出遊戲。
規則敘述	循環{五}{紅}{紅}
規則敘述	當你循環滅絕的宣判時，消滅所有地。
	
顏色／稀有度	紅色／普通
英文牌名	Dragon Breath
中文牌名	龍息
魔法力費用	{一}{紅}
類別	生物結界
收集編號	86/143
規則敘述	受此結界的生物具有敏捷異能。
規則敘述	{紅}：受此結界的生物得+1/+0直到回合結束。
規則敘述	當總魔法力費用大於或等於6的生物進場時，你可以將龍息從你的墳墓場中移回場上，並結附於該生物上。
	
顏色／稀有度	紅色／稀有
英文牌名	Dragon Mage
中文牌名	巨龍法師
魔法力費用	{五}{紅}{紅}
類別	生物～龍／魔法師
收集編號	87/143
力量／防禦力	5/5
規則敘述	飛行
規則敘述	每當巨龍法師對玩家造成戰鬥傷害時，每位玩家各棄掉其手牌，並抽七張牌。
背景敘述	「你將從我號令～不論你寶貴的神智是否應許。」
	
顏色／稀有度	紅色／稀有
英文牌名	Dragon Tyrant
中文牌名	暴君巨龍
魔法力費用	{八}{紅}{紅}
類別	生物～龍
收集編號	88/143
力量／防禦力	6/6
規則敘述	飛行，踐踏
規則敘述	連擊（此生物能造成先攻傷害以及普通傷害。）
規則敘述	在你的維持開始時，除非你支付{紅}{紅}{紅}{紅}，否則犧牲暴君巨龍。
規則敘述	{紅}：暴君巨龍得+1/+0直到回合結束。
	
顏色／稀有度	紅色／非普通
英文牌名	Dragonspeaker Shaman
中文牌名	龍語祭師
魔法力費用	{一}{紅}{紅}
類別	生物～野蠻人
收集編號	89/143
力量／防禦力	2/2
規則敘述	你使用的龍咒語費用減少{二}來使用。
背景敘述	「我等瞭解龍族的熾焰與狂憤。他們瞭解我等的怒火與榮耀。我們將一同編織前所未見的毀滅篇章。」
	
顏色／稀有度	紅色／稀有
英文牌名	Dragonstorm
中文牌名	龍群蔽空
魔法力費用	{八}{紅}
類別	巫術
收集編號	90/143
規則敘述	從你的牌庫中搜尋一張龍牌並將其放置進場。然後將你的牌庫洗牌。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。）
	
顏色／稀有度	紅色／非普通
英文牌名	Enrage
中文牌名	激怒
魔法力費用	{X}{紅}
類別	瞬間
收集編號	91/143
規則敘述	目標生物得+X/+0直到回合結束。
背景敘述	「你不會喜歡我生氣的樣子。」
	
顏色／稀有度	紅色／非普通
英文牌名	Extra Arms
中文牌名	增添武力
魔法力費用	{四}{紅}
類別	生物結界
收集編號	92/143
規則敘述	每當受此結界的生物攻擊時，它對目標生物或玩家造成2點傷害。
背景敘述	「或許添個頭效果才會變好。」
背景敘述	～丘陵嚮導
	
顏色／稀有度	紅色／稀有
英文牌名	Form of the Dragon
中文牌名	化身巨龍
魔法力費用	{四}{紅}{紅}{紅}
類別	結界
收集編號	93/143
規則敘述	在你的維持開始時，化身巨龍對目標生物或玩家造成5點傷害。
規則敘述	在每個回合結束時，你的總生命成為5。
規則敘述	不具飛行異能的生物不能攻擊你。
	
顏色／稀有度	紅色／普通
英文牌名	Goblin Brigand
中文牌名	鬼怪強盜
魔法力費用	{一}{紅}
類別	生物～鬼怪
收集編號	94/143
力量／防禦力	2/2
規則敘述	鬼怪強盜每回合若能攻擊，則必須攻擊。
背景敘述	在斯克山脊崩毀後，鬼怪豎起由繩子和滑輪組成的系統，用來搶劫剩下的東西。
	
顏色／稀有度	紅色／非普通
英文牌名	Goblin Psychopath
中文牌名	病態鬼怪
魔法力費用	{三}{紅}
類別	生物～鬼怪／突變體
收集編號	95/143
力量／防禦力	5/5
規則敘述	每當病態鬼怪進行攻擊或阻擋時，擲一枚硬幣。若你輸掉此擲，則於本回合中，當它下一次將造成戰鬥傷害時，改為對你造成之。
背景敘述	比起牠狂亂的心智，其所引起的破壞倒還不算什麼。
	
顏色／稀有度	紅色／普通
英文牌名	Goblin War Strike
中文牌名	鬼怪戰襲
魔法力費用	{紅}
類別	巫術
收集編號	96/143
規則敘述	鬼怪戰襲對目標玩家造成傷害，其數量等同於由你操控的鬼怪數量。
背景敘述	「開火，瞄準，預備！」
	
顏色／稀有度	紅色／非普通
英文牌名	Goblin Warchief
中文牌名	鬼怪戰酋長
魔法力費用	{一}{紅}{紅}
類別	生物～鬼怪
收集編號	97/143
力量／防禦力	2/2
規則敘述	你使用的鬼怪咒語費用減少{一}來使用。
規則敘述	由你操控的鬼怪具有敏捷異能。
背景敘述	他們自司克山脊如岩漿奔流而下，燃燒並吞噬途經的一切。
	
顏色／稀有度	紅色／稀有
英文牌名	Grip of Chaos
中文牌名	混沌之攫
魔法力費用	{四}{紅}{紅}
類別	結界
收集編號	98/143
規則敘述	每當任一咒語或異能進入堆疊時，若它僅指定單一目標，則為其隨機重選目標。（在所有合法目標間重選）
背景敘述	當世道混沌無序時，賢人與愚者並無分別。
	
顏色／稀有度	紅色／普通
英文牌名	Misguided Rage
中文牌名	歧途怒火
魔法力費用	{二}{紅}
類別	巫術
收集編號	99/143
規則敘述	目標玩家犧牲一個永久物。
背景敘述	當怒火熄滅後，瓦夫才發現他燒了自己的家，毀了自己的武器，還殺了自己的朋友發特。
	
顏色／稀有度	紅色／非普通
英文牌名	Pyrostatic Pillar
中文牌名	電焰柱
魔法力費用	{一}{紅}
類別	結界
收集編號	100/143
規則敘述	每當玩家使用總魔法力費用小於或等於3的咒語時，電焰柱對該玩家造成2點傷害。
	
顏色／稀有度	紅色／普通
英文牌名	Rock Jockey
中文牌名	落岩操縱手
魔法力費用	{二}{紅}
類別	生物～鬼怪
收集編號	101/143
力量／防禦力	3/3
規則敘述	若你本回合使用了地，便不能使用落岩操縱手。
規則敘述	若你本回合使用了落岩操縱手，便不能使用地。
背景敘述	鬼怪不太懂物理，但對掉下去與石頭倒瞭解不少。
	
顏色／稀有度	紅色／普通
英文牌名	Scattershot
中文牌名	濫射
魔法力費用	{二}{紅}
類別	瞬間
收集編號	102/143
規則敘述	濫射對目標生物造成1點傷害。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。你可以為複製品選擇新的目標。）
	
顏色／稀有度	紅色／稀有
英文牌名	Siege-Gang Commander
中文牌名	攻城指揮官
魔法力費用	{三}{紅}{紅}
類別	生物～鬼怪
收集編號	103/143
力量／防禦力	2/2
規則敘述	當攻城指揮官進場時，將三個1/1紅色鬼怪衍生物放置進場。
規則敘述	{一}{紅}，犧牲一個鬼怪：攻城指揮官對目標生物或玩家造成2點傷害。
	
顏色／稀有度	紅色／非普通
英文牌名	Skirk Volcanist
中文牌名	司克火山術士
魔法力費用	{三}{紅}
類別	生物～鬼怪
收集編號	104/143
力量／防禦力	3/1
規則敘述	變身～犧牲兩個山脈。（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當司克火山術士翻回正面時，它對任意數量的目標生物造成共3點傷害，你可以任意分配。
	
顏色／稀有度	紅色／普通
英文牌名	Spark Spray
中文牌名	星火飛濺
魔法力費用	{紅}
類別	瞬間
收集編號	105/143
規則敘述	星火飛濺對目標生物或玩家造成1點傷害。
規則敘述	循環{紅}（{紅}，從你手上棄掉此牌：抽一張牌。）
背景敘述	這是鬼怪唯一接受的淋浴方式。
	
顏色／稀有度	紅色／稀有
英文牌名	Sulfuric Vortex
中文牌名	硫磺旋風
魔法力費用	{一}{紅}{紅}
類別	結界
收集編號	106/143
規則敘述	在每位玩家的維持開始時，硫磺旋風對該玩家造成2點傷害。
規則敘述	若玩家將獲得生命，則改為該玩家未獲得生命。
	
顏色／稀有度	紅色／普通
英文牌名	Torrent of Fire
中文牌名	火焰奔流
魔法力費用	{三}{紅}{紅}
類別	巫術
收集編號	107/143
規則敘述	火焰奔流對目標生物或玩家造成傷害，其數量等同於由你操控的永久物間最高之總魔法力費用。
背景敘述	龍焰能融化任何用來測量它的工具。
	
顏色／稀有度	紅色／普通
英文牌名	Uncontrolled Infestation
中文牌名	繁殖失控
魔法力費用	{一}{紅}
類別	地結界
收集編號	108/143
規則敘述	繁殖失控只能結附於非基本地上。
規則敘述	當受此結界的地成為橫置時，將其消滅。
背景敘述	激流計劃的原址現在只剩下裂片妖，破燒杯，以及海鷗的孤鳴。
	
顏色／稀有度	綠色／普通
英文牌名	Accelerated Mutation
中文牌名	加速變異
魔法力費用	{三}{綠}{綠}
類別	瞬間
收集編號	109/143
規則敘述	目標生物得+X/+X直到回合結束，X為由你操控的永久物間最高之總魔法力費用。
背景敘述	牠的食物變得非常小，而胃變得非常大。
	
顏色／稀有度	綠色／非普通
英文牌名	Alpha Status
中文牌名	領袖地位
魔法力費用	{二}{綠}
類別	生物結界
收集編號	110/143
規則敘述	對受此結界的生物而言，場上每有一個與其具共通之生物類別的其他生物，它便得+2/+2。
背景敘述	最好的領袖是由其隨眾型塑。
	
顏色／稀有度	綠色／稀有
英文牌名	Ambush Commander
中文牌名	伏擊指揮官
魔法力費用	{三}{綠}{綠}
類別	生物～妖精
收集編號	111/143
力量／防禦力	2/2
規則敘述	由你操控的樹林是1/1綠色妖精生物，並且仍然是地。
規則敘述	{一}{綠}，犧牲一個妖精：目標生物得+3/+3直到回合結束。
	
顏色／稀有度	綠色／稀有
英文牌名	Ancient Ooze
中文牌名	遠古流漿
魔法力費用	{五}{綠}{綠}
類別	生物～流漿
收集編號	112/143
力量／防禦力	*/*
規則敘述	遠古流漿的力量和防禦力各等同於其他由你操控的生物總魔法力費用的總和。
背景敘述	亙古常在。
	
顏色／稀有度	綠色／普通
英文牌名	Break Asunder
中文牌名	化為碎片
魔法力費用	{二}{綠}{綠}
類別	巫術
收集編號	113/143
規則敘述	消滅目標神器或結界。
規則敘述	循環{二}（{二}，從你手上棄掉此牌：抽一張牌。）
背景敘述	「這不會帶來好事。」
	
顏色／稀有度	綠色／非普通
英文牌名	Claws of Wirewood
中文牌名	修索林之爪
魔法力費用	{三}{綠}
類別	巫術
收集編號	114/143
規則敘述	修索林之爪對每個具飛行異能的生物與每位玩家各造成3點傷害。
規則敘述	循環{二}（{二}，從你手上棄掉此牌：抽一張牌。）
背景敘述	他們說過樹林有眼睛。他們從沒提過它的爪子。
	
顏色／稀有度	綠色／稀有
英文牌名	Decree of Savagery
中文牌名	蠻勇的宣判
魔法力費用	{七}{綠}{綠}
類別	瞬間
收集編號	115/143
規則敘述	在每個由你操控的生物上各放置四個+1/+1指示物。
規則敘述	循環{四}{綠}{綠}
規則敘述	當你循環蠻勇的宣判時，你可以在目標生物上放置四個+1/+1指示物。
	
顏色／稀有度	綠色／普通
英文牌名	Divergent Growth
中文牌名	趨異生長
魔法力費用	{綠}
類別	瞬間
收集編號	116/143
規則敘述	直到回合結束，由你操控的地獲得「{橫置}：加一點任意顏色的魔法力到你的魔法力池中。"
背景敘述	自然遺忘其法則。
	
顏色／稀有度	綠色／普通
英文牌名	Dragon Fangs
中文牌名	龍牙
魔法力費用	{一}{綠}
類別	生物結界
收集編號	117/143
規則敘述	受此結界的生物得+1/+1並具有踐踏異能。
規則敘述	當總魔法力費用大於或等於6的生物進場時，你可以將龍牙從你的墳墓場中移回場上，並結附於該生物上。
	
顏色／稀有度	綠色／非普通
英文牌名	Elvish Aberration
中文牌名	畸變妖精
魔法力費用	{五}{綠}
類別	生物～妖精／突變體
收集編號	118/143
力量／防禦力	4/5
規則敘述	{橫置}：加{綠}{綠}{綠}到你的魔法力池中。
規則敘述	循環樹林{二}（{二}，從你手上棄掉此牌：從你的牌庫中搜尋一張樹林牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。）
	
顏色／稀有度	綠色／普通
英文牌名	Fierce Empath
中文牌名	好鬥共感者
魔法力費用	{二}{綠}
類別	生物～妖精
收集編號	119/143
力量／防禦力	1/1
規則敘述	當好鬥共感者進場時，你可以從你的牌庫中搜尋一張總魔法力費用大於或等於6的生物牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。
	
顏色／稀有度	綠色／稀有
英文牌名	Forgotten Ancient
中文牌名	遺世先人
魔法力費用	{三}{綠}
類別	生物～元素
收集編號	120/143
力量／防禦力	0/3
規則敘述	每當玩家使用咒語時，你可以在遺世先人上放置一個+1/+1指示物。
規則敘述	在你的維持開始時，你可以將遺世先人上任意數量的+1/+1指示物移至其他生物上。
背景敘述	血湧生命，體散活力。
	
顏色／稀有度	綠色／非普通
英文牌名	Hunting Pack
中文牌名	行獵獸群
魔法力費用	{五}{綠}{綠}
類別	瞬間
收集編號	121/143
規則敘述	將一個4/4綠色野獸衍生物放置進場。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。）
	
顏色／稀有度	綠色／普通
英文牌名	Krosan Drover
中文牌名	克洛薩驅獸人
魔法力費用	{三}{綠}
類別	生物～妖精
收集編號	122/143
力量／防禦力	2/2
規則敘述	由你使用之生物咒語，且總魔法力費用大於或等於6者，其費用減少{二}來使用。
背景敘述	「坐下。」
	
顏色／稀有度	綠色／非普通
英文牌名	Krosan Warchief
中文牌名	克洛薩戰酋長
魔法力費用	{二}{綠}
類別	生物～野獸
收集編號	123/143
力量／防禦力	2/2
規則敘述	你使用的野獸咒語費用減少{一}來使用。
規則敘述	{一}{綠}：重生目標野獸。
背景敘述	牠讓獵物變成獵者。
	
顏色／稀有度	綠色／非普通
英文牌名	Kurgadon
中文牌名	克加敦獸
魔法力費用	{四}{綠}
類別	生物～野獸
收集編號	124/143
力量／防禦力	3/3
規則敘述	每當玩家使用總魔法力費用大於或等於6的生物咒語時，在克加敦獸上放置三個+1/+1指示物。
背景敘述	當牠漫遊於克洛薩境內，樹木靠邊站開，樹葉紛飛逃離，大地瑟瑟顫慄。
	
顏色／稀有度	綠色／非普通
英文牌名	One with Nature
中文牌名	自然調合
魔法力費用	{綠}
類別	生物結界
收集編號	125/143
規則敘述	每當受此結界的生物對玩家造成戰鬥傷害時，你可以從你的牌庫中搜尋一張基本地牌並將其橫置進場。然後將你的牌庫洗牌。
	
顏色／稀有度	綠色／稀有
英文牌名	Primitive Etchings
中文牌名	原始蝕刻
魔法力費用	{二}{綠}{綠}
類別	結界
收集編號	126/143
規則敘述	展示你每回合抽到的第一張牌。每當你以此法展示出生物牌時，抽一張牌。
背景敘述	克洛薩樹上的古老雕刻再度發出力量的光芒。
	
顏色／稀有度	綠色／稀有
英文牌名	Root Elemental
中文牌名	生機元素
魔法力費用	{四}{綠}{綠}
類別	生物～元素
收集編號	127/143
力量／防禦力	6/5
規則敘述	變身{五}{綠}{綠}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當生機元素翻回正面時，你可以從你手上將一張生物牌放置進場。
	
顏色／稀有度	綠色／普通
英文牌名	Sprouting Vines
中文牌名	新綠藤蔓
魔法力費用	{二}{綠}
類別	瞬間
收集編號	128/143
規則敘述	從你的牌庫中搜尋一張基本地牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。
規則敘述	風暴（當你使用此咒語時，本回合於此咒語之前每使用過一個咒語，便將此咒語複製一次。）
	
顏色／稀有度	綠色／普通
英文牌名	Titanic Bulvox
中文牌名	蠻牛巨獸
魔法力費用	{六}{綠}{綠}
類別	生物～野獸
收集編號	129/143
力量／防禦力	7/4
規則敘述	踐踏
規則敘述	變身{四}{綠}{綠}{綠}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
背景敘述	自然法則已然毀棄，貪食者橫行天下。
	
顏色／稀有度	綠色／普通
英文牌名	Treetop Scout
中文牌名	樹頂斥候
魔法力費用	{綠}
類別	生物～妖精
收集編號	130/143
力量／防禦力	1/1
規則敘述	樹頂斥候只能被具飛行異能的生物阻擋。
背景敘述	斥候住在樹頂搖晃而柔韌的家中，有些終其一生未曾碰過地面。
	
顏色／稀有度	綠色／稀有
英文牌名	Upwelling
中文牌名	湧昇
魔法力費用	{三}{綠}
類別	結界
收集編號	131/143
規則敘述	魔法力池在階段或回合結束時不會清空。（此效應阻止魔法力灼傷。）
背景敘述	卡馬爾再度感到映奇寶珠傳來的強烈吸引力，但之前的教訓已讓他學到了很多。
	
顏色／稀有度	綠色／普通
英文牌名	Wirewood Guardian
中文牌名	修索林守護者
魔法力費用	{五}{綠}{綠}
類別	生物～妖精／突變體
收集編號	132/143
力量／防禦力	6/6
規則敘述	循環樹林{二}（{二}，從你手上棄掉此牌：從你的牌庫中搜尋一張樹林牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。）
背景敘述	如樹般高大，而且八成有血緣關係。
	
顏色／稀有度	綠色／非普通
英文牌名	Wirewood Symbiote
中文牌名	修索林共生體
魔法力費用	{綠}
類別	生物～昆蟲
收集編號	133/143
力量／防禦力	1/1
規則敘述	將一個由你操控的妖精移回其擁有者手上：重置目標生物。此異能每回合中只能使用一次。
背景敘述	以疲勞感為食。
	
顏色／稀有度	綠色／普通
英文牌名	Woodcloaker
中文牌名	木篷師
魔法力費用	{五}{綠}
類別	生物～妖精
收集編號	134/143
力量／防禦力	3/3
規則敘述	變身{二}{綠}{綠}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當木篷師翻回正面時，目標生物獲得踐踏異能直到回合結束。
	
顏色／稀有度	綠色／稀有
英文牌名	Xantid Swarm
中文牌名	沾體蟲群
魔法力費用	{綠}
類別	生物～昆蟲
收集編號	135/143
力量／防禦力	0/1
規則敘述	飛行
規則敘述	每當沾體蟲群攻擊時，防禦玩家本回合不能使用咒語。
背景敘述	沾上身後，除了扯掉他們，你不會有其他的念頭。
	
顏色／稀有度	多色／稀有
英文牌名	Bladewing the Risen
中文牌名	復生者鋒翼
魔法力費用	{三}{黑}{黑}{紅}{紅}
類別	生物～傳奇龍
收集編號	136/143
力量／防禦力	4/4
規則敘述	飛行
規則敘述	當復生者鋒翼進場時，你可以將目標在你墳墓場中的龍牌移回場上。
規則敘述	{黑}{紅}：所有龍得+1/+1直到回合結束。
	
顏色／稀有度	多色／非普通
英文牌名	Edgewalker
中文牌名	逸界僧
魔法力費用	{一}{白}{黑}
類別	生物～僧侶
收集編號	137/143
力量／防禦力	2/2
規則敘述	你使用的僧侶咒語費用減少{白}{黑}來使用。此效應僅減少你所支付的有色魔法力。（舉例來說，若你使用魔法力費用為{一}{白}的僧侶，你只需支付{一}）
	
顏色／稀有度	多色／稀有
英文牌名	Karona, False God
中文牌名	偽神卡若娜
魔法力費用	{一}{白}{藍}{黑}{紅}{綠}
類別	生物～傳奇
收集編號	138/143
力量／防禦力	5/5
規則敘述	敏捷
規則敘述	在每位玩家的維持開始時，該玩家重置偽神卡若娜並獲得其操控權。
規則敘述	每當卡若娜攻擊時，選擇一種生物類別，該類別的生物得+3/+3直到回合結束。
	
顏色／稀有度	多色／稀有
英文牌名	Sliver Overlord
中文牌名	裂片妖大王
魔法力費用	{白}{藍}{黑}{紅}{綠}
類別	生物～傳奇裂片妖／突變體
收集編號	139/143
力量／防禦力	7/7
規則敘述	{三}：從你的牌庫中搜尋一張裂片妖牌，展示該牌，並置於你手上。然後將你的牌庫洗牌。
規則敘述	{三}：獲得目標裂片妖的操控權。（此效應不因回合結束而終止）
背景敘述	進化的終點。
	
顏色／稀有度	神器／非普通
英文牌名	Ark of Blight
中文牌名	枯萎秘櫃
魔法力費用	{二}
類別	神器
收集編號	140/143
規則敘述	{三}，{橫置}，犧牲枯萎秘櫃：消滅目標地。
背景敘述	一旦開啟，便將整個地景自心靈與地圖中抹去。
	
顏色／稀有度	神器／非普通
英文牌名	Proteus Machine
中文牌名	百變械
魔法力費用	{三}
類別	神器生物
收集編號	141/143
力量／防禦力	2/2
規則敘述	變身{零}（你可牌面朝下地使用此牌並支付{三}，將其當成2/2生物。可隨時支付其變身費用使其翻回正面。）
規則敘述	當百變械翻回正面時，選擇一種生物類別，它成為該類別。（此效應不因回合結束而終止）
	
顏色／稀有度	神器／稀有
英文牌名	Stabilizer
中文牌名	穩流儀
魔法力費用	{二}
類別	神器
收集編號	142/143
規則敘述	玩家不能循環牌。
背景敘述	「先別這麼想。」
背景敘述	～激流餘生者潘明
	
顏色／稀有度	地／非普通
英文牌名	Temple of the False God
中文牌名	偽神殿堂
類別	地
收集編號	143/143
規則敘述	{橫置}：加{二}到你的魔法力池中。你只可以於你操控五張或更多地時使用此異能。
背景敘述	無誠不靈。

